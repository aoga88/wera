<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- ENCABEZADO QUE MUESTRA EL LOGO Y LA BARRA DE MENU  -->
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <title>SISTEMA DE CONTROL INMOBILIARIO</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name=”keywords” content=”redirección automática”>
    <link rel="shortcut icon" href="css/images/favicon.ico" /><!-- URL DEL LOGOTIPO -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- HOJA DE ESTILO -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/><!-- VALIDACIONES -->
    <link rel="stylesheet" href="css/jquery-ui.css" /><!--CALENDARIO-->
    <script src="js/jquery-1.6.2.min.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT QUE PERMITE MOSTRAR EFECTOS Y JQUERY -->
    <script src="js/jquery.jcarousel.min.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT QUE PERMITE  -->
    <script src="js/jquery.lavalamp.min.js" type="text/javascript" charset="utf-8"></script><!-- MOSTRAR EL EFECTO DE LAS IMAGENES EN MOVIMIENTO -->
    <script src="js/functions.js" type="text/javascript" charset="utf-8"></script><!-- Y OTROS -->
    <script src="js/jquery.easing.1.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.easing.compatibility.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA QUE VALIDE LOS CAMPOS -->
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA QUE VALIDE LOS CAMPOS -->
    <script src="js/validarNuevo.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA VALIDAR (FORMNUEVO) -->
    <script src="js/consultacombos.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA CONSULTAR COMBOS -->
    <script src="js/franja.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA COMPARAR CHECKBOX (FRANJA) -->
    <script src="js/sumasup.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA SUMAR SUPERFICIES -->
    <script src="js/propietarios.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA COMPARAR CHECKBOX (PROPIETARIOS) --> 
    <script src="js/agregarpropietario.js" type="text/javascript" charset="utf-8"></script> <!-- SCRIPT PARA AGREGAR PROPIETARIOS -->
    <script src="js/mensajesparpadeo.js" type="text/javascript" charset="utf-8"></script> <!-- SCRIPT PARA QUE MUESTRE MENSAJES -->      
    <script src="js/busqueda.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA COMPARAR CHECKBOX (BUSQUEDA) -->
    <script src="js/busquedainput.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA COMPARAR CHECKBOXPROPIETARIOS (BUSQUEDA) -->
    <script src="js/validarBusqIndex.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA VALIDAR (FORMBUSCARESP) -->    
    <script src="js/validarPrestamos.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA VALIDAR (FORMPRESTAMO) -->
    <script src="js/jquery-ui.js" type="text/javascript" charset="utf-8"></script><!--SCRIPT PARA EL CALENDARIO-->
    <script src="js/prestamo.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA COMPARAR CHECKBOX (pPRESTAMOS) -->
    <script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
    </script>
</head>
<body>
    <!-- Inicio Wrapper -->
    <div id="fondo-arriba">
        <div class="fondo-abajo">
            <!-- Inicio Shell -->
            <div class="shell">
                <!-- Inicio Encabezado -->
                <div id="encabezado">
                    <h1 id="logo"><a class="notexto" href="" title="Logotipo">Logotipo</a></h1>
                    <!-- Inicio Barra -->
                    <div id="barra">
                        <ul>
                            <li><a onclick="location='Inicio.php'" title="Inicio" >Inicio </a></li>   
                            <li><a onclick="location='Nuevo.php'" title="Nuevo">Nuevo </a></li>
                            <li><a onclick="location='Buscar.php'" title="Buscar">Buscar</a></li>
                            <li><a onclick="location='Permisos.php'" title="Permisos">Permisos</a></li>
                            <li><a onclick="location='Prestamos.php'" title="Prestamos">Pr&eacute;stamos</a></li>
                        </ul>
                        <div class="cl">&nbsp;</div>
                    </div>
                    <!-- Fin Barra -->
                </div>
                <!-- Fin Encabezado -->
		