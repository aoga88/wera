<!-- Inicio Deslizador Imagenes (Slider)-->
<!-- muestra 3 imágenes con diferente texto -->
<div id="deslizador">
    <span class="marco marco-arriizq">&nbsp;</span>
    <span class="marco marco-arrider">&nbsp;</span>
    <span class="marco marco-abajizq">&nbsp;</span>
    <span class="marco marco-abajder">&nbsp;</span>
    <ul class="deslizador-elementos">
        <li>
            <h2>FILATEQ</h2>
            <div class="cl">&nbsp;</div>
            <div class="slide-entry"></div>
            <img src="css/images/desliz-img1.jpg" alt="Slide Image" />            
            <div class="cl">&nbsp;</div>
        </li>
        <li>
            <h2>Fideicomiso </h2>
            <div class="cl">&nbsp;</div>
            <div class="slide-entry"></div>
            <img src="css/images/desliz-img3.jpg" alt="Slide Image" />
            <div class="cl">&nbsp;</div>
        </li>
        <li>
            <h2>Lago de Tequesquitengo</h2>
            <div class="cl">&nbsp;</div>
            <div class="slide-entry"></div>
            <img src="css/images/desliz-img2.jpg" alt="Slide Image" /> 
            <div class="cl">&nbsp;</div>
        </li>
    </ul>
    <div class="cl">&nbsp;</div>
    <div class="deslizador-nav">
        <div class="nav-interior"></div>
        
    </div>
    
</div>
<!-- Fin Deslizador Imagenes (Slider) -->