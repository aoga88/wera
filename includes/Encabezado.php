<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- ENCABEZADO QUE SOLO MUESTRA EL LOGOTIPO Y LA BARRA AZUL  -->
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <title>SISTEMA DE CONTROL INMOBILIARIO</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="css/images/favicon.ico" /><!-- URL DEL LOGOTIPO -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- HOJA DE ESTILO -->
    <link rel="stylesheet" href="css/validationEngineLogin.jquery.css" type="text/css"/><!-- VALIDACIONES -->
    <script src="js/jquery-1.6.2.min.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT QUE PERMITE MOSTRAR EFECTOS Y JQUERY -->
    <script src="js/jquery.jcarousel.min.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT QUE PERMITE -->
    <script src="js/jquery.lavalamp.min.js" type="text/javascript" charset="utf-8"></script><!-- MOSTRAR EL EFECTO DE LAS IMAGENES EN MOVIMIENTO -->
    <script src="js/functions.js" type="text/javascript" charset="utf-8"></script><!-- Y OTROS -->
    <script src="js/jquery.easing.1.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.easing.compatibility.js" type="text/javascript" charset="utf-8"></script> 
    <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA QUE VALIDE LOS CAMPOS -->
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA QUE VALIDE LOS CAMPOS -->
    <script src="js/validarLogin.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA VALIDAR (FORMLOGIN) -->
    <script src="js/mensajesparpadeo.js" type="text/javascript" charset="utf-8"></script><!-- SCRIPT PARA QUE MUESTRE MENSAJES -->  
</head>
<body>
    <!-- Inicio Wrapper -->
    <div id="fondo-arriba">
        <div class="fondo-abajo">
            <!-- Inicio Shell -->
            <div class="shell">
                <!-- Inicio Encabezado -->
                <div id="encabezado">
                    <h1 id="logo"><a class="notexto" href="#" title="Logotipo">Logotipo</a></h1>
                    <!-- Inicio Barra -->
                    <div id="barra">
                        <ul>	</ul>
                        <div class="cl">&nbsp;</div>
                    </div>
                    <!-- Fin Barra -->
                </div>
                <!-- Fin Encabezado -->
				