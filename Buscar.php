<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
unset($_SESSION['resultado_busqueda']); //TERMINA LA BUSQUEDA ESPECÍFICA Y LIMPIA LOS RESULTADOS
unset($_SESSION['resultado_busquedaSeg']); //TERMINA LA BUSQUEDA GENERAL Y LIMPIA LOS RESULTADOS
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- - VERIFICA SI SE GUARDARON LOS DATOS PARA MANDAR MSJ - -->
<?php
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {
        case 0:
            $mensaje0="NO ENCONTRADO";
            break;
        /*case 1:
            $mensaje1="BUSQUEDA EXITOSA ";
            break;*/
        default:
            break;
    }
} 
if(isset($mensaje0)){
  echo "<div class='error mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
/*if(isset($mensaje1)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}*/
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2>BUSCAR</h2>
                <div class="cl">&nbsp;</div>              
            </div>
            <!-- Inicio Contenido -->
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div><p>&nbsp;&nbsp;</p></div>
            <div class="formulario" align="center">
                <!-- ---------- BUSQUEDA ESP O GRAL --------------- -->
                <div>
                    <table class="centrar" width="80%">
                        <tr>
                            <td style="background: #cccccc ;"><img src="css/images/buscar.png" onClick="agregarBuscador()" title="Buscar" alt=">>>"/></td><td style="background: #2683d1 ;" onClick="agregarBuscador()" title="Buscar">BÚSQUEDA ESPECÍFICA</td>
                            <td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>
                            <td style="background: #cccccc ;"><img src="css/images/buscar.png" onClick="agregarBuscadorSeg()" title="Buscar" alt=">>>"/></td><td style="background: #2683d1 ;" onClick="agregarBuscadorSeg()" title="Buscar">BÚSQUEDA GENERAL</td> 
                        </tr>
                    </table>
                </div>
                <!-- ----------- FORMBUSCARESP ---------------- -->
                <div>
                <div>&nbsp;&nbsp;</div>    
                <form name="formBuscarEsp" id="formBuscarEsp" class="formulario" action="Buscar_archivo.php" method="POST" enctype="multipart/form-data">
                    <div id="formIndexada" style="display:none;"> 
                        <fieldset><legend></legend><p>&nbsp;</p>
                            <label>SECCI&Oacute;N: </label>
                            <select id="seccion" name="seccion" class="validate[required]">
                            <option value=" ">SELECCIONAR...</option>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>MANZANA: </label>
                            <select id="manzana" name="manzana" class="validate[required]">
                            <option value=" ">SELECCIONAR...</option>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>LOTE: </label><input type="text" id="lote" name="lote" class="validate[required] text-input" /><p>&nbsp;</p>                           
                        </fieldset><p>&nbsp;</p> 
                        <span class="boton"><input class="submit" type="submit" name="buscador" value="Buscar"/></span>                
                    </div> 
                </form>                    
                </div>
                <!-- --------------- FORMBUSCARGRAL --------------- -->
                <form name="formBuscarGral" id="formBuscarGral" class="formulario" action="Buscar_archivos.php" method="POST" enctype="multipart/form-data">
                <div id="formIndizada" style="display:none;">
                    <fieldset><legend></legend><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                        <div><hr align="center" width="800" size="2" /></div>
                        <legend>DATOS GENERALES</legend>
                        <div><hr align="center" width="800" size="2" /></div><p>&nbsp;</p>
                        <label>SECCI&Oacute;N: </label>
                        <select id="secciones" name="secciones" >
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>MANZANA: </label>
                        <select id="manzanas" name="manzanas" >
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>LOTE: </label><input type="text" id="lote" name="lote" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>NO. EXPEDIENTE: </label><input type="text" id="noexpediente" name="noexpediente" class="validate[required] text-input" /><p>&nbsp;</p> 
                        <div>
                            <label>SITUACI&Oacute;N JUR&Iacute;DICA: </label>                            
                            <select id="situacionjur" name="situacionjur"> 
                            <option value="">SELECCIONAR...</option>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>SITUACI&Oacute;N F&Iacute;SICA: </label> 
                            <select id="situacionfis" name="situacionfis"> 
                            <option value="">SELECCIONAR...</option>
                            </select>                        
                        </div><p>&nbsp;</p>
                        <div>
                            <label>FRANJA: </label>
                            <select id="franja" name="franja">
                            <option value=" ">SELECCIONAR...</option>
                            <option value="AZUL">AZUL</option>
                            <option value="NARANJA">NARANJA</option>
                            <option value="VERDE">VERDE</option>                        
                            </select>
                       </div><div><p>&nbsp</p><p>&nbsp</p><p>&nbsp</p></div>
                       <div><hr align="center" width="800" size="2" /></div>                       
                       <legend>PROPIETARIO</legend>
                       <div><hr align="center" width="800" size="2" /></div><p>&nbsp;</p>
                       <label>PERSONA F&Iacute;SICA</label><input type="radio" id="propietario0" name="propietario" value="PERSONA F&Iacute;SICA" onclick="MostrarInput()" />
                       <label>PERSONA MORAL</label><input type="radio" id="propietario1" name="propietario" value="PERSONA MORAL" onclick="MostrarInput()"  />
                       <label>PROPIEDAD DE FILATEQ</label><input type="radio" id="propietario2" name="propietario" value="FILATEQ" onclick="MostrarInput()" />
                       <div id="Fisica" style="display:none;"><p>&nbsp;</p> 
                           <table id="fisica">
                               <tr>
                                   <td><label>NOMBRE</label></td> <td><input type="text" id="nombre" name="nombre" /> </td>
                               </tr>
                               <tr>
                                   <td><label>APELLIDO PATERNO</label></td> <td><input type="text" id="ap" name="ap" /> </td>
                               </tr>
                               <tr>
                                   <td><label>APELLIDO MATERNO</label></td>  <td><input type="text" id="am" name="am" /> </td>  
                               </tr>
                           </table>                           
                       </div>                    
                       <div class="otro"id="Moral" style="display:none;"><p>&nbsp;</p>
                                <label>RAZ&Oacute;N SOCIAL: </label><input type="text" name="razonsocial" />                             
                       </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p> 
                   </fieldset><p>&nbsp;</p> 
                        <span class="boton"><input class="submit" type="submit" name="buscador" value="Buscar"/></span>                
                    </fieldset>     
                </div>
                </form>
                <!-- ------------- RESULTADOS BUSQESP ------------- -->
                <div>&nbsp;&nbsp;</div>
                <div>               
                    <?php if(isset($_SESSION['resultado_busqueda'])):?>
                    <table class="tabla2" width="100%">
                        <tr >
                            <th>NO.</th>
                            <th>SECCION</th>
                            <th>MANZANA</th>
                            <th>LOTE</th>
                            <th>NO.EXPEDIENTE</th>
                            <th>SITUACIÓN JURÍDICA</th>
                            <th>SITUACIÓN FÍSICA</th>
                            <th>FRANJA</th>                            
                            <th>MÁS DETALLES<th>                        
                        <?php $result = $_SESSION['resultado_busqueda'];
                        foreach($result as $indice => $objUsuario):
                            $objUsuarioTmp = new Entrada();
                            $objUsuarioTmp = unserialize($objUsuario);?>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getFila()?></td>
                            <td><?php echo $objUsuarioTmp->getIdseccion()?></td>
                            <td><?php echo $objUsuarioTmp->getIdmanzana()?></td>
                            <td><?php echo $objUsuarioTmp->getLote()?></td>
                            <td><?php echo $objUsuarioTmp->getNoexpediente()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionjur()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionfis()?></td>
                            <td><?php echo $objUsuarioTmp->getFranja()?></td>
                            <td><a href="Expediente.php?valor=<?php echo $objUsuarioTmp->getIdterreno(); ?>" >MÁS</a></td>
                        </tr>
                        <?php endforeach;
                        unset($_SESSION['resultado_busqueda']); ?>
                        <tr>
                            <td style="background: #ffffff; color: #ffffff; ">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>TOTAL</th>                            
                        </tr>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getTotal()?></td>
                        </tr>
                        
                    </table>                    
                    <?php endif; ?>                                       
                </div>
                <!-- --------------RESULTADOS BUSQGRAL-------------- -->
                <div>               
                    <?php if(isset($_SESSION['resultado_busquedaSeg'])):?>
                    <table class="tabla2" width="100%">
                        <tr >
                            <th>NO.</th>
                            <th>SECCION</th>
                            <th>MANZANA</th>
                            <th>LOTE</th>
                            <th>NO.EXPEDIENTE</th>
                            <th>SITUACIÓN JURÍDICA</th>
                            <th>SITUACIÓN FÍSICA</th>
                            <th>FRANJA</th>                            
                            <th>MÁS DETALLES<th>                        
                        <?php $result = $_SESSION['resultado_busquedaSeg'];
                        foreach($result as $indice => $objUsuario):
                            $objUsuarioTmp = new Entrada();
                            $objUsuarioTmp = unserialize($objUsuario);?>
                        <tr>                            
                            <td><?php echo $objUsuarioTmp->getFila()?></td>
                            <td><?php echo $objUsuarioTmp->getIdseccion()?></td>
                            <td><?php echo $objUsuarioTmp->getIdmanzana()?></td>
                            <td><?php echo $objUsuarioTmp->getLote()?></td>
                            <td><?php echo $objUsuarioTmp->getNoexpediente()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionjur()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionfis()?></td>
                            <td><?php echo $objUsuarioTmp->getFranja()?></td>
                            <td><a href="Expediente.php">MÁS</a></td>
                        </tr>
                        <?php endforeach;
                        unset($_SESSION['resultado_busquedaSeg']); ?>
                    </table>                    
                    <?php endif; ?>                                       
                </div>
                <!-- ------------------------------------------- -->
            </div><p>&nbsp;</p>
            <div class="cl">&nbsp;</div><p>&nbsp;</p>
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>