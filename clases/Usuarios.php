<?php
//----VARIABLES DE (ACCESO.PHP) CON MÉTODOS GET Y SET----//
class Usuarios {
    private $idusuario;
    private $nombre;
    private $usuario;
    private $password;
        
    public  $tipo_usuario;

    public function getIdusuario() {
        return $this->idusuario;
    }

    public function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
}
//-----------------------------------------------------//
?>