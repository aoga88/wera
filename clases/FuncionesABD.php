<?php
//----RECIBE PARAMETROS Y ACCESA A LA BASE DE DATOS----// 
class FuncionesABD {
     //------------------------------------------------------------------------------------------------------------//
     //FUNCION QUE REALIZA UNA CONSULTA PARA VER SI EL USUARIO EXISTE (Acceso_autentificar.php)
     public function Acceso_autentificar($usuario,$password) {
        $db = new MySQL();  
        $consulta = $db->consulta("SELECT id_usuario, nombre, usuario, password, tipo FROM usuarios u, tipo_usuario tu WHERE u.idtipo_usuario=tu.idtipo_usuario AND usuario='$usuario' AND password='$password' ");  
        if(($row=  mysql_fetch_array($consulta))){
            //SI ENTRA AQUI ES POR QUE EL USUARIO Y PASSWORD ESTAN BIEN
            $objUsuario=new Usuarios();
            $objUsuario->tipo_usuario=$row['tipo'];
            $objUsuario->setIdusuario($row['id_usuario']);
            $objUsuario->setNombre($row['nombre']);
            $objUsuario->setUsuario($row['usuario']);
            $objUsuario->setPassword($row['password']);
            return $objUsuario;            
        }else{
            return NULL;
        }
    }    
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE INSERTA VALORES A LA BASE DE DATOS(TABLAS: ARCHIVOS, TERRENOS) (Nuevo_crear.php)
    public function Nuevo_crear(&$objArchivo, $entradaObj, $entradaSupA, $entradaSupN,$entradaSupV, $entradaPerMor, $entradaPerFis) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO PARA ARCHIVO
        $titulo=$objArchivo->getTitulo();
        $nombrearchivo=$objArchivo->getNombrearchivo(); 
        $contenido=$objArchivo->getContenido();
        $tipo=$objArchivo->getTipo();
        $tamanio=$objArchivo->getTamanio();
        $tamaniounidad=$objArchivo->getTamaniounidad();
        $resultadoarchivo= $db->registra("INSERT INTO archivos VALUES (null,'$titulo','$contenido','$tamanio[0]','$tipo','$nombrearchivo','$tamanio[1]', default) ");       
        $franja=$entradaObj->getFranja();
        if($franja=='AZUL'){
            $regularizada=$entradaSupA->getRegularizada();
            $deslindada=$entradaSupA->getDeslindada();
            $extinta=$entradaSupA->getExtinta();
            $zonafed=$entradaSupA->getZonafed();
            $exc_filateq=$entradaSupA->getExcfilateq();
            $superfis=$entradaSupA->getSuperfis();
            $resultsupazul=$db->registra("INSERT INTO superficie_azul VALUES (null,'$regularizada','$deslindada','$extinta','$zonafed','$exc_filateq','$superfis',default)");
            $idsupazul=  mysql_insert_id();
            $result=$db->consulta("SELECT * FROM superficie_azul");
            $tabla=mysql_field_table($result,0);
            $resultadosupazul= $db->registra("INSERT INTO superficies VALUES (null,'$tabla','$idsupazul') ");
            $idsuperficie= mysql_insert_id();
        }
        if($franja=='NARANJA'){
            $regularizada=$entradaSupN->getRegularizada();
            $deslindada=$entradaSupN->getDeslindada();
            $extinta=$entradaSupN->getExtinta();
            $acreditada=$entradaSupN->getAcreditada();
            $filateq=$entradaSupN->getFilateq();
            $zonafed=$entradaSupN->getZonafed();
            $exc_filateq=$entradaSupN->getExcfilateq();
            $superfis=$entradaSupN->getSuperfis();
            $resultsupnar=$db->registra("INSERT INTO superficie_naranja VALUES (null,'$regularizada','$deslindada','$extinta','$acreditada','$filateq','$zonafed','$exc_filateq','$superfis',default)");
            $idsupnar=  mysql_insert_id();
            $result=$db->consulta("SELECT * FROM superficie_naranja");
            $tabla=mysql_field_table($result,0);
            $resultadosupnar= $db->registra("INSERT INTO superficies VALUES (null,'$tabla','$idsupnar') ");
            $idsuperficie= mysql_insert_id();
        }        
        if($franja=='VERDE')
        {
            $regularizada=$entradaSupV->getRegularizada();
            $deslindada=$entradaSupV->getDeslindada();
            $extinta=$entradaSupV->getExtinta();
            $acreditada=$entradaSupV->getAcreditada();
            $filateq=$entradaSupV->getFilateq();
            $zonafed=$entradaSupV->getZonafed();
            $exc_filateq=$entradaSupV->getExcfilateq();
            $superfis=$entradaSupV->getSuperfis();
            $resultsupver=$db->registra("INSERT INTO superficie_verde VALUES (null,'$regularizada','$deslindada','$extinta','$acreditada','$filateq','$zonafed','$exc_filateq','$superfis',default)");
            $idsupver=  mysql_insert_id();
            $result=$db->consulta("SELECT * FROM superficie_verde");
            $tabla=mysql_field_table($result,0);
            $resultadosupver= $db->registra("INSERT INTO superficies VALUES (null,'$tabla','$idsupver') ");
            $idsuperficie= mysql_insert_id();
        }        
        //PERSONAFISICA
        $nombre=$entradaPerFis->getPfisnombre();
        $ap=$entradaPerFis->getPfisap();
        $am=$entradaPerFis->getPfisam();
        $telefono=$entradaPerFis->getPfistelefono();
        $cp=$entradaPerFis->getPfiscp();
        $direccion=$entradaPerFis->getPfisdireccion();
        //PERSONA MORAL
        $razonsocial=$entradaPerMor->getPmorrazonsocial();
        $telefono=$entradaPerMor->getPmortelefono();
        $direccion=$entradaPerMor->getPmordireccion();
        
        if (isset ($nombre,$ap,$am,$telefono,$cp,$direccion)){         
            $resultperfis= $db->registra("INSERT INTO compradores_fisicos VALUES (null,'$nombre','$ap', '$am','$telefono','$cp','$direccion', default) ");
            $idperfis=  mysql_insert_id();
            $result=$db->consulta("SELECT * FROM compradores_fisicos");
            $tabla=mysql_field_table($result,0);
            $resultadopermor= $db->registra("INSERT INTO compradores VALUES (null,'$tabla','$idperfis') ");
            $idcomprador= mysql_insert_id();
        }

        else{
            $resultpermor= $db->registra("INSERT INTO compradores_morales VALUES (null,'$razonsocial','$telefono', '$direccion', default) ");
            $idpermor=  mysql_insert_id();
            $result=$db->consulta("SELECT * FROM compradores_morales");
            $tabla=mysql_field_table($result,0);
            $resultadopermor= $db->registra("INSERT INTO compradores VALUES (null,'$tabla','$idpermor') ");
            $idcomprador= mysql_insert_id();
        }
        
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO PARA DATOS DEL TERRENO
        $idarchivo=mysql_insert_id();
        $idseccion=$entradaObj->getIdseccion();
        $idmanzana=$entradaObj->getIdmanzana();
        $lote=$entradaObj->getLote();
        $noexpediente=$entradaObj->getNoexpediente();
        $idsituacionjur=$entradaObj->getIdsituacionjur();
        $idsituacionfis=$entradaObj->getIdsituacionfis();        
        $observaciones=$entradaObj->getObservaciones();        
        $resultadoterreno= $db->registra("INSERT INTO terrenos VALUES (null,'$idseccion','$idmanzana','$lote','$noexpediente','$idsituacionjur','$idsituacionfis','$franja','$idsuperficie','$idcomprador','$idarchivo','$observaciones')");         
    }
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA VER SI EL ARCHIVO EXISTE (Buscar_archivo.php)(Prestamos_alta.php)
    public function Buscar_archivo($seccion, $manzana, $lote) {
        $db = new MySQL();  
        $consulta = $db->consulta("SELECT *,nombresec,nombreman,nombresitjur,nombresitfis FROM terrenos ter,secciones sec, manzanas man, situacionjuridica jur,situacionfisica fis WHERE ter.id_seccion LIKE '%$seccion%' AND sec.id_seccion='$seccion' AND ter.id_manzana LIKE'%$manzana%' AND man.id_manzana='$manzana' AND lote LIKE '%$lote%' AND ter.id_situacionjur=jur.id_situacionjur AND ter.id_situacionfis=fis.id_situacionfis ");    
        $terrenos = array();
        $numero=1;
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new Entrada();
                $objUsuario->setFila($numero);
                $objUsuario->setIdterreno($row['id_terreno']);
                $objUsuario->setIdseccion($row['nombresec']);
                $objUsuario->setIdmanzana($row['nombreman']);
                $objUsuario->setLote($row['lote']);
                $objUsuario->setNoexpediente($row['noexpediente']);
                $objUsuario->setIdsituacionjur($row['nombresitjur']);
                $objUsuario->setIdsituacionfis($row['nombresitfis']);
                $objUsuario->setFranja($row['franja']);               
                $total=$numero++;                
                $objUsuario->setTotal($total);
                $terrenos[] = serialize($objUsuario);
            }
            return $terrenos;
        }else{
            return NULL;
        }
    }
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA VER SI LOS ARCHIVOS EXISTEN (Buscar_archivos.php)
    public function Buscar_archivos(&$entradaObj) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO
        $idseccion=$entradaObj->getIdseccion();
        $idmanzana=$entradaObj->getIdmanzana();
        $lote=$entradaObj->getLote();
        $noexpediente=$entradaObj->getNoexpediente();
        $idsituacionjur=$entradaObj->getIdsituacionjur();
        $idsituacionfis=$entradaObj->getIdsituacionfis();
        $franja=$entradaObj->getFranja();  
        $consulta= "SELECT * FROM terrenos WHERE ";
        if (isset ($idseccion,$idmanzana,$lote,$franja))
            {
            $condicion="";             
            $condicion=($condicion=="")?" id_seccion LIKE '%$idseccion%' ":  " AND id_seccion LIKE '%$idseccion%' ";
            $condicion=($condicion=="")?" id_manzana LIKE '%$idmanzana%' ":  " AND id_manzana LIKE '%$idmanzana%' ";
            $condicion=($condicion=="")?" lote LIKE '%$lote%' ":  " AND lote LIKE '%$lote%' ";
            $condicion=($condicion=="")?" franja LIKE '%$franja%' ":  " AND franja LIKE '%$franja%' ";
            }
        if (isset ($idseccion,$idmanzana,$lote,$franja))
            {
            $condicion="";             
            $condicion=($condicion=="")?" id_seccion LIKE '%$idseccion%' ":  " OR id_seccion LIKE '%$idseccion%' ";
            $condicion=($condicion=="")?" id_manzana LIKE '%$idmanzana%' ":  " OR id_manzana LIKE '%$idmanzana%' ";
            $condicion=($condicion=="")?" lote LIKE '%$lote%' ":  " OR lote LIKE '%$lote%' ";
            $condicion=($condicion=="")?" franja LIKE '%$franja%' ":  " OR franja LIKE '%$franja%' ";
            }
        $consulta = $db->consulta($consulta.$condicion); 
        //$consulta = $db->consulta("SELECT * FROM terrenos WHERE id_seccion LIKE '%$idseccion%' OR id_manzana LIKE '%$idmanzana%' OR lote LIKE '%$lote%' OR noexpediente LIKE '%$noexpediente%' OR franja LIKE '%$franja%' OR id_situacionjur LIKE '%$idsituacionjur%' OR id_situacionfis LIKE '%$idsituacionfis%' ORDER BY id_manzana ");    
        $terrenos = array();
        $numero=1;
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new Entrada();
                $objUsuario->setFila($numero);
                $objUsuario->setIdterreno($row['id_terreno']);
                $objUsuario->setIdseccion($row['nombresec']);
                $objUsuario->setIdmanzana($row['nombreman']);
                $objUsuario->setLote($row['lote']);
                $objUsuario->setNoexpediente($row['noexpediente']);
                $objUsuario->setIdsituacionjur($row['nombresitjur']);
                $objUsuario->setIdsituacionfis($row['nombresitfis']);
                $objUsuario->setFranja($row['franja']);               
                $total=$numero++;                
                $objUsuario->setTotal($total);
                $terrenos[] = serialize($objUsuario);
            }
            return $terrenos;
        }else{
            return NULL;
        }
    }
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA VER SI EL ARCHIVO ESPECIFICO EXISTE (EXPEDIENTE.PHP)
    public function Expediente_buscar_archivoesp(&$valor) { 
        $db = new MySQL();  
        $consulta = $db->consulta("SELECT *,nombresec,nombreman,nombresitjur,nombresitfis FROM terrenos ter,secciones sec, manzanas man, situacionjuridica jur,situacionfisica fis, archivos ar WHERE ter.id_terreno='$valor' AND ter.id_seccion=sec.id_seccion AND ter.id_manzana=man.id_manzana AND ter.id_situacionjur=jur.id_situacionjur AND ter.id_situacionfis=fis.id_situacionfis  AND ter.id_archivo=ar.id_archivo ");    
        $res = $db->consulta("select docs.*, CASE docs.tipo                
			WHEN 'image/png' then 
				'image'
			WHEN 'image/jpg' then 
				'image'
			WHEN 'image/gif' then 
				'image'
			WHEN 'image/jpeg' then 
				'image'                          
			ELSE 
				'file' 
		END as display FROM archivos as docs WHERE docs.id_archivo='1'");
        $terrenos = array();
        $numero=1;
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new Entrada();
                $objUsuario->setFila($numero);
                $objUsuario->setIdterreno($row['id_terreno']);
                $objUsuario->setIdseccion($row['nombresec']);                
                $objUsuario->setIdmanzana($row['nombreman']);
                $objUsuario->setLote($row['lote']);
                $objUsuario->setNoexpediente($row['noexpediente']);
                $objUsuario->setIdsituacionjur($row['nombresitjur']);
                $objUsuario->setIdsituacionfis($row['nombresitfis']);
                $objUsuario->setFranja($row['franja']);
                $objUsuario->setObservaciones($row['observaciones']);  
                $objUsuario->setIdarchivo($row['id_archivo']);
                $terrenos[] = serialize($objUsuario);
                $numero++;                
            }
            while ($obj=mysql_fetch_object($res)) {
	
	switch ($obj->display){
		case "image":
			echo "<div>
					<a href='getfile.php?id_archivo={$obj->id_archivo}'>
						<img src='getfile.php?id_documento={$obj->id_archivo}' alt='$obj->titulo' />
					</a>
				</div><hr />";
			break;
		case "file":
			echo "<div>
					<a href='getfile.php?id_archivo={$obj->id_archivo}'>$obj->titulo</a>
				</div><hr />";
			break;			
	}
    }  
            return $terrenos;
        }else{
            return NULL;
        }
        
    }
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE INSERTA VALORES A LA BASE DE DATOS(TABLA: USUARIOS) (Permisos_alta.php)
    public function Permisos_alta(&$entradaObjUser) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO
        $idtipousuario=$entradaObjUser->getIdtipousuario();
        $nombre=$entradaObjUser->getNombre();
        $ap=$entradaObjUser->getApellidop();
        $am=$entradaObjUser->getApellidom();
        $cargo=$entradaObjUser->getCargo();
        $usuario=$entradaObjUser->getUsuario();
        $password=$entradaObjUser->getPassword();
        $resultado= $db->registra("INSERT INTO usuarios VALUES (null,'$idtipousuario','$nombre','$ap','$am','$cargo','$usuario',sha1('$password'),default )");         
    } 
    //------------------------------------------------------------------------------------------------------------//    
    //FUNCION QUE ELIMINAS VALORES A LA BASE DE DATOS(TABLA: USUARIOS) (Permisos_baja.php)
    public function Permisos_baja(&$valor) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO
        //$idusuario=$objUsuario->getIdusuario();
        $resultado= $db->consulta("DELETE FROM usuarios WHERE id_usuario='$valor'"); 
    }
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA VER QUE USUARIOS EXISTEN (PERMISOS.PHP)
    public function Permisos_consultar_usuarios() {
        $db = new MySQL();          
        $consulta = $db->consulta("SELECT *,nombre FROM usuarios u,tipo_usuario tu WHERE u.idtipo_usuario=tu.idtipo_usuario ORDER BY u.idtipo_usuario");    
        $usuarios = array();
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new EntradaUser();               
                $objUsuario->setIdusuario($row['id_usuario']);
                $objUsuario->setIdtipousuario($row['tipo']);
                $objUsuario->setNombre($row['nombre']);
                $objUsuario->setApellidop($row['apellidop']);
                $objUsuario->setApellidom($row['apellidom']);
                $objUsuario->setCargo($row['cargo']);
                $objUsuario->setUsuario($row['usuario']);
                $usuarios[] = serialize($objUsuario);
            }
            return $usuarios;
        }else{
            return NULL;
        }
    }                           
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA EL USUARIO SELECCIONADO (PERMISOSMODIFICAR.php)
    public function PermisosModificar_buscar_usuario(&$valor) {
        $db = new MySQL();          
        $consulta = $db->consulta("SELECT *,nombre FROM usuarios u,tipo_usuario tu WHERE u.id_usuario='$valor' AND u.idtipo_usuario=tu.idtipo_usuario");    
        $usuarios = array();
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new EntradaUser();               
                $objUsuario->setIdusuario($row['id_usuario']);
                $objUsuario->setIdtipousuario($row['tipo']);
                $objUsuario->setNombre($row['nombre']);
                $objUsuario->setApellidop($row['apellidop']);
                $objUsuario->setApellidom($row['apellidom']);
                $objUsuario->setCargo($row['cargo']);
                $objUsuario->setUsuario($row['usuario']);
                $usuarios[] = serialize($objUsuario);
            }
            return $usuarios;
        }else{
            return NULL;
        }
    }   
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE INSERTA VALORES A LA BASE DE DATOS(TABLA: USUARIOS) (Permisos_guardarmodif.php)
    public function Permisos_guardarmodif(&$entradaObjUser) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO
        $id=$entradaObjUser->getIdusuario();
        $idtipousuario=$entradaObjUser->getIdtipousuario();
        $nombre=$entradaObjUser->getNombre();
        $ap=$entradaObjUser->getApellidop();
        $am=$entradaObjUser->getApellidom();
        $cargo=$entradaObjUser->getCargo();
        $usuario=$entradaObjUser->getUsuario();
        $password=$entradaObjUser->getPassword();
        $resultado= $db->registra("UPDATE usuarios SET idtipo_usuario='$idtipousuario',nombre='$nombre',apellidop='$ap',apellidom='$am',cargo='$cargo',usuario='$usuario',password=sha1('$password') WHERE id_usuario='$id' ");         
    } 
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA EL USUARIO SELECCIONADO  (Prestamos_alta.php)
    public function Prestamos_alta_buscar_usuarionom(&$nombre) {
        $db = new MySQL();          
        $consulta = $db->consulta("SELECT *,nombre FROM usuarios u,tipo_usuario tu WHERE u.nombre='$nombre' AND u.idtipo_usuario=tu.idtipo_usuario");    
        $usuarios = array();
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objUsuario=new EntradaUser();               
                $objUsuario->setIdusuario($row['id_usuario']);
                $objUsuario->setIdtipousuario($row['tipo']);
                $objUsuario->setNombre($row['nombre']);
                $objUsuario->setApellidop($row['apellidop']);
                $objUsuario->setApellidom($row['apellidom']);
                $objUsuario->setCargo($row['cargo']);
                $objUsuario->setUsuario($row['usuario']);
                $usuarios[] = serialize($objUsuario);
            }
            return $usuarios;
        }else{
            return NULL;
        }
    } 
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE BUSCA LA FECHA DE DEVOLUCION PARA VER SI YA PUEDE REGISTRARLO (Prestamos_guardarmodif.php)
    public function Prestamos_guardarmodif_buscar_prestamodevol(&$ticket) {
        $db = new MySQL();  
        $consulta = $db->consulta("SELECT fechadevol FROM prestamos WHERE no_ticket='$ticket' ");    
        $prestamos = array();
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objPrestamo=new Entrada();               
                $objPrestamo->setDevolucion($row['fechadevol']);
                $prestamos[] = serialize($objPrestamo);
            }
            return $prestamos;
        }else{
            return NULL;
        }        
    }      
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE INSERTA VALORES A LA BASE DE DATOS(TABLA: TERRENOS)  (Prestamos_alta.php)
    public function Prestamos_alta($idter,$iduser,$fechaprestamo,$fechadevolucion,$password) {
        $db = new MySQL();          
        $resultado= $db->registra("INSERT INTO prestamos VALUES (null,'$password','$iduser','$idter','$fechaprestamo','$fechadevolucion',default)");         
        $consulta = $db->consulta("SELECT *, nombre,noexpediente FROM prestamos pre,usuarios us,terrenos ter WHERE no_ticket='$password' AND pre.id_terreno=ter.id_terreno AND pre.id_usuario=us.id_usuario ");    
        $prestamos = array();
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objPrestamo=new Entrada(); 
                $objPrestamo->setTicket($row['no_ticket']);
                $objPrestamo->setNombre($row['nombre']);
                $objPrestamo->setAp($row['apellidop']);
                $objPrestamo->setNoexpediente($row['noexpediente']);
                $objPrestamo->setPrestamo($row['fechaprest']);
                $objPrestamo->setDevolucion($row['fechadevol']);
                $objPrestamo->setEstado($row['estado']);              
                $objPrestamo->setTotal($total);                
                $prestamos[] = serialize ($objPrestamo);
            }            
            return $prestamos;
        }else{
            return NULL;
        }        
    }   
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE GUARDA CUANDO SE DEVUELVE UN DOCUMENTO (Prestamos_guardarmodif.php)
    public function Prestamos_guardarmodif(&$ticket) {
        $db = new MySQL();  
        //SE OBTIENEN LOS VALORES DEL OBJETO QUE SE DARA POR PARAMETRO
        $resultado= $db->registra("UPDATE prestamos SET estado='0' WHERE no_ticket='$ticket' ");         
    }     
    //------------------------------------------------------------------------------------------------------------//
    //FUNCION QUE REALIZA UNA CONSULTA PARA VER QUE USUARIOS EXISTEN   (PRESTAMOSLISTAR.php)
    public function Prestamos_listar() {
        $db = new MySQL();          
        $consulta = $db->consulta("SELECT *,nombre,noexpediente FROM prestamos pre,usuarios us,terrenos ter WHERE pre.id_terreno=ter.id_terreno AND pre.id_usuario=us.id_usuario ORDER BY pre.fechaprest ");    
        $prestamos = array();
        $numero=1;
        if(mysql_num_rows($consulta) != 0){
            while($row=  mysql_fetch_array($consulta)){
                $objPrestamo=new Entrada(); 
                $objPrestamo->setFila($numero);
                $objPrestamo->setTicket($row['no_ticket']);
                $objPrestamo->setNombre($row['nombre']);
                $objPrestamo->setAp($row['apellidop']);
                $objPrestamo->setNoexpediente($row['noexpediente']);
                $objPrestamo->setPrestamo($row['fechaprest']);
                $objPrestamo->setDevolucion($row['fechadevol']);
                $objPrestamo->setEstado($row['estado']);
                $total=$numero++;                
                $objPrestamo->setTotal($total);                
                $prestamos[] = serialize ($objPrestamo);
            }            
            return $prestamos;
        }else{
            return NULL;
        }
    }
    //------------------------------------------------------------------------------------------------------------// 
}

?>