<?php
//----------------------------- VARIABLES DE USUARIO ----------------------------//
class EntradaUser {
//-------------------------------------------------------------------------------// 
    private $idusuario;
    private $idtipousuario;
    private $nombre;
    private $apellidop;
    private $apellidom;
    private $cargo;
    private $usuario;
    private $password;
    
    public function getIdusuario() {
        return $this->idusuario;
    }

    public function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

    public function getIdtipousuario() {
        return $this->idtipousuario;
    }

    public function setIdtipousuario($idtipousuario) {
        $this->idtipousuario = $idtipousuario;
    }
    
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getApellidop() {
        return $this->apellidop;
    }

    public function setApellidop($apellidop) {
        $this->apellidop = $apellidop;
    }

    public function getApellidom() {
        return $this->apellidom;
    }

    public function setApellidom($apellidom) {
        $this->apellidom = $apellidom;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
//-------------------------------------------------------------------------------//
} 
?>