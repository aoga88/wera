<?php
//----- VARIABLES DE TERRENOS, ARCHIVOS Y PRESTAMOS -------//
class Entrada {
    //--------------------TERRENOS------------------------//
    private $total;
    private $fila;
    private $idterreno;
    private $idseccion;
    private $idmanzana;
    private $lote;
    private $idsituacionjur;
    private $idsituacionfis;
    private $noexpediente;
    private $franja;
    private $observaciones;  
    //---------------------SUPERFICIES---------------------//
    private $regularizada;
    private $deslindada;
    private $extinta;
    private $acreditada;
    private $filateq;
    private $zonafed;
    private $excfilateq;
    private $superfis;            
    //------------------PERSONA FISICA--------------------//
    private $pfisnombre;
    private $pfisap;
    private $pfisam;
    private $pfistelefono;
    private $pfiscp;
    private $pfisdireccion;
    //------------------PERSONA MORAL---------------------//
    private $pmorrazonsocial;
    private $pmortelefono;
    private $pmordireccion;
    //--------------------ARCHIVOS------------------------//
    private $idarchivo;
    private $titulo;
    private $nombrearchivo;
    private $contenido;
    private $tipo;
    private $tamanio;
    private $tamaniounidad;
    //--------------------PRESTAMOS------------------------//
    private $nombre;
    private $ap;
    private $ticket;
    private $prestamo;
    private $devolucion;  
    private $estado;
    //--------------------TERRENOS------------------------//
    public function getTotal() {
        return $this->total;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function getFila() {
        return $this->fila;
    }

    public function setFila($fila) {
        $this->fila = $fila;
    }
        
    public function getIdterreno() {
        return $this->idterreno;
    }

    public function setIdterreno($idterreno) {
        $this->idterreno = $idterreno;
    }

    public function getIdseccion() {
        return $this->idseccion;
    }

    public function setIdseccion($idseccion) {
        $this->idseccion = $idseccion;
    }
    
    public function getIdmanzana() {
        return $this->idmanzana;
    }

    public function setIdmanzana($idmanzana) {
        $this->idmanzana = $idmanzana;
    }
    
    public function getLote() {
        return $this->lote;
    }

    public function setLote($lote) {
        $this->lote = $lote;
    }
    
    public function getNoexpediente() {
        return $this->noexpediente;
    }

    public function setNoexpediente($noexpediente) {
        $this->noexpediente = $noexpediente;
    }
    
    public function getIdsituacionjur() {
        return $this->idsituacionjur;
    }

    public function setIdsituacionjur($idsituacionjur) {
        $this->idsituacionjur = $idsituacionjur;
    }

    public function getIdsituacionfis() {
        return $this->idsituacionfis;
    }

    public function setIdsituacionfis($idsituacionfis) {
        $this->idsituacionfis = $idsituacionfis;
    }
    
    public function getFranja() {
        return $this->franja;
    }

    public function setFranja($franja) {
        $this->franja = $franja;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }    
    //---------------------------------------------------//
    
    //---------------------SUPERFICIES---------------------//
    public function getRegularizada() {
        return $this->regularizada;
    }

    public function setRegularizada($regularizada) {
        $this->regularizada = $regularizada;
    }

    public function getDeslindada() {
        return $this->deslindada;
    }

    public function setDeslindada($deslindada) {
        $this->deslindada = $deslindada;
    }

    public function getExtinta() {
        return $this->extinta;
    }

    public function setExtinta($extinta) {
        $this->extinta = $extinta;
    }

    public function getAcreditada() {
        return $this->acreditada;
    }

    public function setAcreditada($acreditada) {
        $this->acreditada = $acreditada;
    }

    public function getFilateq() {
        return $this->filateq;
    }

    public function setFilateq($filateq) {
        $this->filateq = $filateq;
    }

    public function getZonafed() {
        return $this->zonafed;
    }

    public function setZonafed($zonafed) {
        $this->zonafed = $zonafed;
    }

    public function getExcfilateq() {
        return $this->excfilateq;
    }

    public function setExcfilateq($excfilateq) {
        $this->excfilateq = $excfilateq;
    }
    public function getSuperfis() {
        return $this->superfis;
    }

    public function setSuperfis($superfis) {
        $this->superfis = $superfis;
    }
    //---------------------------------------------------//
    
    //------------------PERSONA FISICA-------------------//
    public function getPfisnombre() {
        return $this->pfisnombre;
    }

    public function setPfisnombre($pfisnombre) {
        $this->pfisnombre = $pfisnombre;
    }

    public function getPfisap() {
        return $this->pfisap;
    }

    public function setPfisap($pfisap) {
        $this->pfisap = $pfisap;
    }

    public function getPfisam() {
        return $this->pfisam;
    }

    public function setPfisam($pfisam) {
        $this->pfisam = $pfisam;
    }

    public function getPfistelefono() {
        return $this->pfistelefono;
    }

    public function setPfistelefono($pfistelefono) {
        $this->pfistelefono = $pfistelefono;
    }

    public function getPfiscp() {
        return $this->pfiscp;
    }

    public function setPfiscp($pfiscp) {
        $this->pfiscp = $pfiscp;
    }

    public function getPfisdireccion() {
        return $this->pfisdireccion;
    }

    public function setPfisdireccion($pfisdireccion) {
        $this->pfisdireccion = $pfisdireccion;
    }
    //---------------------------------------------------//
    
    //------------------PERSONA MORAL---------------------//
    public function getPmorrazonsocial() {
        return $this->pmorrazonsocial;
    }

    public function setPmorrazonsocial($pmorrazonsocial) {
        $this->pmorrazonsocial = $pmorrazonsocial;
    }

    public function getPmortelefono() {
        return $this->pmortelefono;
    }

    public function setPmortelefono($pmortelefono) {
        $this->pmortelefono = $pmortelefono;
    }

    public function getPmordireccion() {
        return $this->pmordireccion;
    }

    public function setPmordireccion($pmordireccion) {
        $this->pmordireccion = $pmordireccion;
    }
    //---------------------------------------------------//
    
    //--------------------ARCHIVOS------------------------//
    public function getIdarchivo() {
        return $this->idarchivo;
    }

    public function setIdarchivo($idarchivo) {
        $this->idarchivo = $idarchivo;
    }
    
    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getNombrearchivo() {
        return $this->nombrearchivo;
    }

    public function setNombrearchivo($nombrearchivo) {
        $this->nombrearchivo = $nombrearchivo;
    }
    public function getContenido() {
        return $this->contenido;
    }

    public function setContenido($contenido) {
        $this->contenido = $contenido;
    }
    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }   
    public function getTamanio() {
        return $this->tamanio;
    }

    public function setTamanio($tamanio) {
        $this->tamanio = $tamanio;
    }
    public function getTamaniounidad() {
        return $this->tamaniounidad;
    }

    public function setTamaniounidad($tamaniounidad) {
        $this->tamaniounidad = $tamaniounidad;
    }  
    //----------------------------------------------------//
    
    //--------------------PRESTAMOS------------------------//
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function getAp() {
        return $this->ap;
    }

    public function setAp($ap) {
        $this->ap = $ap;
    }

    public function getTicket() {
       return $this->ticket;
    }

    public function setTicket($ticket) {
       $this->ticket = $ticket;
    }

    public function getPrestamo() {
        return $this->prestamo;
    }

    public function setPrestamo($prestamo) {
        $this->prestamo = $prestamo;
    }

    public function getDevolucion() {
        return $this->devolucion;
    }

    public function setDevolucion($devolucion) {
        $this->devolucion = $devolucion;
    }
    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }
    //---------------------------------------------------<//
}
?>