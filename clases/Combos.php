<?php
//CLASE QUE CONTIENE 4 FUNCIONES LAS CUALES RELLENAN LOS COMBOS (NUEVO.PHP) CONSULTANDO A LA BASE DE DATOS
class Combos extends MySQL{
    
    var $code = "";
    //-------------------------------------------------------------------------------//  
    //FUNCIÓN QUE HACE UN SELECT DE LAS SECCIONES
    function cargarSecciones(){
        $consulta = parent::consulta("SELECT id_seccion,nombresec FROM secciones ");
        $num_total_registros = parent::num_rows($consulta);
        if($num_total_registros>0){
            $secciones= array();
            while($seccion = parent::fetch_assoc($consulta)){
                $id = $seccion["id_seccion"];
                $nombre = $seccion["nombresec"];	
                $secciones[$id]=$nombre;
            }
            return $secciones;        
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------------------------// 
    //FUNCIÓN QUE HACE UN SELECT DE LAS MANZANAS 
    function cargarManzanas(){
        $consulta = parent::consulta("SELECT id_manzana,nombreman FROM manzanas WHERE id_seccion = '".$this->code."'");
        $num_total_registros = parent::num_rows($consulta);
        if($num_total_registros>0){
            $manzanas = array();
            while($manzana = parent::fetch_assoc($consulta)){
                $id = $manzana["id_manzana"];
                $nombre = $manzana["nombreman"];
                $manzanas[$id]=$nombre;
            }
            return $manzanas;
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------------------------// 
    //FUNCIÓN QUE HACE UN SELECT DE LA SITUACION JURIDICA
    function cargarSitJur(){
        $consulta = parent::consulta("SELECT id_situacionjur,nombresitjur FROM situacionjuridica ");
        $num_total_registros = parent::num_rows($consulta);
        if($num_total_registros>0){
            $sitjurs= array();
            while($situacionjur = parent::fetch_assoc($consulta)){
                $id = $situacionjur["id_situacionjur"];
                $nombre = $situacionjur["nombresitjur"];	
                $sitjurs[$id]=$nombre;
            }
            return $sitjurs;        
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------------------------// 
    //FUNCIÓN QUE HACE UN SELECT DE LA SITUACION FISICA 
    function cargarSitFis(){
        $consulta = parent::consulta("SELECT id_situacionfis,nombresitfis FROM situacionfisica ");
        $num_total_registros = parent::num_rows($consulta);
        if($num_total_registros>0){
            $sitfis= array();
            while($situacionfis = parent::fetch_assoc($consulta)){
                $id = $situacionfis["id_situacionfis"];
                $nombre = $situacionfis["nombresitfis"];	
                $sitfis[$id]=$nombre;
            }
            return $sitfis;        
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------------------------// 
    //FUNCIÓN QUE HACE UN SELECT DE LOS TIPOS DE USUARIOS
    function cargarTipoUser(){
        $consulta = parent::consulta("SELECT * FROM tipo_usuario ");
        $num_total_registros = parent::num_rows($consulta);
        if($num_total_registros>0){
            $tipousuarios= array();
            while($tipouser = parent::fetch_assoc($consulta)){
                $id = $tipouser["idtipo_usuario"];
                $nombre = $tipouser["tipo"];	
                $tipousuarios[$id]=$nombre;
            }
            return $tipousuarios;        
        }else{
            return false;
        }
    }
}

?>