<?php
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/Entrada.php';
include_once 'clases/FuncionesABD.php';
//--------------------- ALMACENA LOS ARCHIVOS ------------------------//
function filesize_format($bytes, $format = '', $force = ''){
    $bytes=(float)$bytes;
    if ($bytes <1024){
        $numero=number_format($bytes, 0, '.', ',');
        return array($numero,"B");
    }
    if ($bytes <1048576){
        $numero=number_format($bytes/1024, 2, '.', ',');
	return array($numero,"KBs");
    }
    if ($bytes>= 1048576){
	$numero=number_format($bytes/1048576, 2, '.', ',');
	return array($numero,"MB");
    }
}

//EN ESTA VARIABLE ALMACENAMOS EL NOMBRE TEMPORAL QUE SE LE ASIGNO ESTE NOMBRE ES GENERADO POR EL SERVIDOR
//ASI QUE SI NUESTRO ARCHIVO SE LLAMA foto.jpg el tmp_name NO SERÁ foto.jpg SINO UN NOMBRE COMO SI12349712983.tmp POR DECIR UN EJEMPLO
$archivo = $_FILES["archivo"]["tmp_name"];
//DEFINIMOS UN ARRAY PARA ALMACENAR EL TAMAÑO DEL ARCHIVO
$tamanio=array();
//OBTENEMOS EL TAMAÑO DEL ARCHIVO
$tamanio = $_FILES["archivo"]["size"];
//OBTENEMOS EL TIPO MIME DEL ARCHIVO
$tipo = $_FILES["archivo"]["type"];
//OBTENEMOS EL NOMBRE REAL DEL ARCHIVO AQUI SI SERIA foto.jpg
$nombre_archivo = $_FILES["archivo"]["name"];
//EXTRAEMOS LOS DATOS DEL REQUEST
extract($_REQUEST);
//VERIFICAMOS DE NUEVO QUE SE SELECCIONO ALGUN ARCHIVO

if ( $archivo != "none" ){
    //ABRIMOS EL ARCHIVO EN MODO SOLO LECTURA VERIFICAMOS EL TAÑANO DEL ARCHIVO
    $fp = fopen($archivo, "rb");
    //LEEMOS EL CONTENIDO DEL ARCHIVO
    $contenido = fread($fp, $tamanio);
    //CON LA FUNCION addslashes AGREGAMOS UN \ A CADA COMILLA SIMPLE ' PORQUE DE OTRA MANERA NOS MARCARIA ERROR A LA HORA DE REALIZAR EL INSERT EN NUESTRA TABLA
    $contenido = addslashes($contenido);
    //CERRAMOS EL ARCHIVO
    fclose($fp);
    //VERIFICAMOS EL TAMAÑO DEL ARCHIVO
    if ($tamanio <1048576){
        //HACEMOS LA CONVERSION PARA PODER GUARDAR SI EL TAMAÑO ESTA EN b ó MB
	$tamanio=filesize_format($tamanio);
	}
    if (($tipo=='application/pdf' || $tipo=='image/jpeg') ||  $tipo=='image/png'){
        $titulo=$_POST['titulo'];
        $objArchivo=new Entrada();
        $objArchivo->setTitulo($titulo);
        $objArchivo->setContenido($contenido);
        $objArchivo->setNombrearchivo($nombre_archivo);
        $objArchivo->setTipo($tipo);
        $objArchivo->setTamanio($tamanio);        
        $objArchivo->setTamaniounidad($tamanio);
    }else{
        //SE REDIRECCIONA A (NUEVO.PHP) MENSAJE 2: "ERROR DE IMAGEN INTENTElO NUEVAMENTE"       
        header("Location: Nuevo.php?mensaje=2");  
    }
}
//----------------------------------------------------------------------//

//------------------- ALMACENA DATOS DEL TERRENO -----------------------//
//---- REVISA QUE EXISTAN LOS VALORES ENVIADOS POR EL FORMULARIO ----//
if (isset($_POST['seccion']) && $_POST['manzana'] && $_POST['lote'] && $_POST['noexpediente'] && $_POST['situacionjur'] && $_POST['situacionfis'] && $_POST['franja'] ){
    //SE OBTIENEN TODOS LOS DATOS ENVIADOS POR EL FORMULARIO
    $idseccion = $_POST['seccion'];
    $idmanzana = $_POST['manzana'];
    $lote= $_POST['lote'];
      $lote = strtoupper( $lote );
    $noexpediente= $_POST['noexpediente'];
      $noexpediente = strtoupper( $noexpediente );
    $idsituacionjur= $_POST['situacionjur'];
    $idsituacionfis= $_POST['situacionfis'];
    $franja= $_POST['franja'];
    $observaciones= $_POST['observaciones'];
      $observaciones = strtoupper( $observaciones );
      
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON
    $entradaObj = new Entrada();
    $entradaObj->setIdseccion($idseccion);
    $entradaObj->setIdmanzana($idmanzana);
    $entradaObj->setLote($lote);
    $entradaObj->setNoexpediente($noexpediente);
    $entradaObj->setIdsituacionjur($idsituacionjur);
    $entradaObj->setIdsituacionfis($idsituacionfis);
    $entradaObj->setFranja($franja);
    $entradaObj->setObservaciones($observaciones);
    //------------------------------------------------------------// 
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON (SUPERFICIE_AZUL)
    $aregularizada=$_POST['aregularizada'];
    $adeslindada=$_POST['adeslindada'];
    $aextinta=$_POST['aextinta'];
    $azonafedlac=$_POST['azonafedlac'];
    $aexcfilateq=$_POST['aexcfilateq'];    
    $asuperfis=$_POST['afisica'];
    
    $entradaSupA= new Entrada();
    $entradaSupA->setRegularizada($aregularizada);
    $entradaSupA->setDeslindada($adeslindada);
    $entradaSupA->setExtinta($aextinta);
    $entradaSupA->setZonafed($azonafedlac);
    $entradaSupA->setExcfilateq($aexcfilateq);
    $entradaSupA->setSuperfis($asuperfis); 
    
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON (SUPERFICIE_NARANJA)
    $nregularizada=$_POST['nregularizada'];
    $ndeslindada=$_POST['ndeslindada'];
    $nextinta=$_POST['nextinta'];
    $nacreditada=$_POST['nacreditada'];
    $nfilateq=$_POST['nfilateq'];
    $nzonafed=$_POST['nzonafed'];
    $nexc_filateq=$_POST['nexcfilateq'];
    $nsuperfis=$_POST['nfisica'];
    
    $entradaSupN= new Entrada();
    $entradaSupN->setRegularizada($nregularizada);
    $entradaSupN->setDeslindada($ndeslindada);
    $entradaSupN->setExtinta($nextinta);
    $entradaSupN->setAcreditada($nacreditada);
    $entradaSupN->setFilateq($nfilateq);
    $entradaSupN->setZonafed($nzonafed);
    $entradaSupN->setExcfilateq($nexc_filateq);
    $entradaSupN->setSuperfis($nsuperfis);   
    
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON (SUPERFICIE_VERDE)
    $vregularizada=$_POST['vregularizada'];
    $vdeslindada=$_POST['vdeslindada'];
    $vextinta=$_POST['vextinta'];
    $vacreditada=$_POST['vacreditada'];
    $vfilateq=$_POST['vfilateq'];
    $vzonafed=$_POST['vzonafed'];
    $vexc_filateq=$_POST['vexcfilateq'];
    $vsuperfis=$_POST['vfisica'];
    
    $entradaSupV= new Entrada();
    $entradaSupV->setRegularizada($vregularizada);
    $entradaSupV->setDeslindada($vdeslindada);
    $entradaSupV->setExtinta($vextinta);
    $entradaSupV->setAcreditada($vacreditada);
    $entradaSupV->setFilateq($vfilateq);
    $entradaSupV->setZonafed($vzonafed);
    $entradaSupV->setExcfilateq($vexc_filateq);
    $entradaSupV->setSuperfis($vsuperfis);
    //------------------------------------------------------------//
    
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON (PERSONA_FISICA) 
    $nombre= $_POST['nombre'];
    $ap= $_POST['ap'];
    $am= $_POST['am'];
    $telefono= $_POST['telefono'];
    $cp= $_POST['cp'];
    $direccion= $_POST['direccion'];
    
    $entradaPerFis= new Entrada();
    $entradaPerFis->setPfisnombre($nombre);
    $entradaPerFis->setPfisap($ap);
    $entradaPerFis->setPfisam($am);
    $entradaPerFis->setPfistelefono($telefono);
    $entradaPerFis->setPfiscp($cp);
    $entradaPerFis->setPfisdireccion($direccion);
    
    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON (PERSONA_MORAL)   
    $razonsocial= $_POST['razonsocial'];
    $telefono= $_POST['telefono'];
    $direccion= $_POST['direccion'];
  
    $entradaPerMor = new Entrada();
    $entradaPerMor->setPmorrazonsocial($razonsocial);
    $entradaPerMor->setPmortelefono($telefono);
    $entradaPerMor->setPmordireccion($direccion);
    //------------------------------------------------------------//
    
    //SE MANDA LLAMAR A LA FUNCION 
    $entradaDALObj = new FuncionesABD();
    $resultado=$entradaDALObj->Nuevo_crear(&$objArchivo,$entradaObj,$entradaSupA,$entradaSupN,$entradaSupV,$entradaPerMor,$entradaPerFis);
     //SE REDIRECCIONA A (NUEVO.PHP) MENSAJE 1: "ALMACENADO EXITOSAMENTE"
        header("Location: Nuevo.php?mensaje=1");
 }else{
     //SE REDIRECCIONA A (NUEVO.PHP) MENSAJE 0: "ERROR AL ALMACENARSE INTENTELO NUEVAMENTE"       
     header("Location: Nuevo.php?mensaje=0");  
    }
//--------------------------------------------------------------------//
?>