<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'includes/Encabezado.php' ?>
<?php require 'includes/DeslizadorImg.php' ?>
<!-- -------- VERIFICA SI SE LOGEO Y MANDA MENSAJE ------- -->
<?php 
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {
        case 0:
            $mensaje0="USUARIO O PASSWORD INCORRECTOS";
            break;
        case 1:
            $mensaje1="INICIE SESION PARA PODER VER LA PÁGINA";
            break;
        default:
            break;
    }
} 

if(isset($mensaje0)){
  echo "<div class='error mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
if(isset($mensaje1)){
  echo "<div class='alerta mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <!-- Inicio Barra_sitio -->
        <div id="barra_sitio">
            <div class="barra_sitio-exterior">
                <div class="barra_sitio-contenido">
                    <ul>
                        <li class="fichero_enlacevi�t">
                            <div class="titulo">
                                <h2><a title="Acceso">ACCESO</a></h2>
                            <div class="cl">&nbsp;</div>              
                            </div>     
                            <!-- ---------------- FORMLOGIN ---------------- -->
                            <form name="formLogin" action="Acceso_autentificar.php" method="POST" id="formLogin" accept-charset="utf-8">
                                <div>&nbsp;<label>USUARIO:</label><input type="text" id="usuario" name="usuario" class="validate[required] text-input"/></div>
                                <div>&nbsp;<label>CONTRASE&Ntilde;A:</label><input type="password" id="password" name="password" class="validate[required] text-input" /></div>
                                <span class="boton"><input class="submit" type="submit" value="Entrar" /></span>
                            </form>
                            <!-- ------------------------------------------- -->
                        </li>
                    </ul>
                    <div class="cl">&nbsp;</div>
                </div>
            </div>
            <div class="barra_sitio-abajo">&nbsp;</div>
        </div>
        <!-- Fin Barra de Sitio -->
        <!-- Inicio Contenido -->
        <div><p>&nbsp;</p><p>&nbsp;</p></div>
        <div id="contenido" class="centrar">            
            <img src="css/images/candado.png" alt="Usuario" />
            <div class="post">
                <div class="contenido"></div>
            </div>
        </div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div> 
<!--Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>				