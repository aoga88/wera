<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- - VERIFICA SI SE GUARDARON LOS DATOS PARA MANDAR MSJ - -->
<?php
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {
        case 0:
            $mensaje0="NO EXISTE EL EXPEDIENTE";            
            break;
        case 1:
            $mensaje1="EL PRÉSTAMO HA SIDO REGISTRADO EXITOSAMENTE !!!";
            break;
        case 2:
            $mensaje2="ERROR DE PRÉSTAMO...INTENTELO NUEVAMENTE";
            break; 
        case 3:
            $mensaje3="LA DEVOLUCIÓN HA SIDO REGISTRADA EXITOSAMENTE !!!";
            break; 
        case 4:
            $mensaje4="ERROR DE DEVOLUCIÓN...INTENTELO NUEVAMENTE";
            break; 
        default:
            break;
    }
} 

if(isset($mensaje0)){
  echo "<div class='error mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
if(isset($mensaje1)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}
if(isset($mensaje2)){
  echo "<div class='error mensajes'>";
  echo "$mensaje2";
  echo "</div>";
}
if(isset($mensaje3)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje3";
  echo "</div>";
}
if(isset($mensaje4)){
  echo "<div class='error mensajes'>";
  echo "$mensaje4";
  echo "</div>";
}
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2>PRÉSTAMOS</h2>
                <div class="cl">&nbsp;</div>              
            </div>            
            <!-- Inicio Contenido -->
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div><p>&nbsp;&nbsp;</p></div>
            <div class="formulario" align="center">
                <!-- ---------- OPCION PRÉSTAMOS O DEVOLUCIONES --------------- -->
                <div>
                    <table class="centrar" width="80%">
                        <tr>
                            <td style="background: #cccccc ;"><img src="css/images/up.png" onClick="agregarPrestamos()" title="Préstamos" alt=">>>"/></td><td style="background: #2683d1 ;" onClick="agregarPrestamos()" title="Buscar">PRÉSTAMOS</td>
                            <td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>
                            <td style="background: #cccccc ;"><img src="css/images/down.png" onClick="agregarDevoluciones()" title="Devoluciones" alt=">>>"/></td><td style="background: #2683d1 ;" onClick="agregarDevoluciones()" title="Buscar">DEVOLUCIONES</td> 
                        </tr>
                    </table>
                </div>
                <!-- ----------- FORMPRESTAMOS---------------- -->
                <div>&nbsp;&nbsp;</div>    
                <form name="formPrestamo" id="formPrestamo" class="formulario" action="Prestamos_alta.php" method="POST" enctype="multipart/form-data">
                    <div id="formPrestamos" style="display:none;">
                        <!-- -------- FORMPRESTAMO ------------ -->                 
                    <fieldset><legend>&nbsp;SELECCIONAR ARCHIVO</legend><p>&nbsp;</p>
                        <label>SECCI&Oacute;N: </label>
                        <select id="seccion" name="seccion" class="validate[required]">
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>MANZANA: </label>
                        <select id="manzana" name="manzana" class="validate[required]">
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>LOTE: </label><input type="text" id="lote" name="lote" class="validate[required] text-input" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </fieldset><p>&nbsp;</p> 
                    <!-- ------------------------------------------- -->
                    <fieldset><legend>&nbsp;SOLICITUD</legend><p>&nbsp;</p>                     
                       <div>
                           <label>FECHA DE PR&Eacute;STAMO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label><input type="text" id="prestamo" name="prestamo" value="<?php echo $fechaActuall=  date("Y/m/d") ?>" readonly="readonly" />
                       </div>
                       <div>                              
                           <label>FECHA DE DEVOLUCI&Oacute;N:&nbsp;&nbsp;</label><input type="text" id="datepicker" name="devolucion" class="validate[required,future[Now]] text-input" readonly="readonly"/>       
                       </div>
                    </fieldset><p>&nbsp;</p>
                    <!-- ------------------------------------------- -->
                    <span class="boton"><input class="submit" type="submit" name="retirar" value="Retirar"/></span>            
                </div>    
                </form>
            </div> 
                <!-- --------------- FORMDEVOLUCIONES --------------- -->                
                <form class="form" name="formDevolucion" id="formBuscarGral" action="Prestamos_guardarmodif.php" method="POST" enctype="multipart/form-data">
                <div  class="centrar" id="formDevoluciones" style="display:none;">
                    <fieldset><legend></legend><p>&nbsp;</p>      
                        <label>NO. TICKET: </label><input type="text" id="noticket" name="noticket" class="validate[required] text-input" /><p>&nbsp;</p>               
                    </fieldset><p>&nbsp;</p>    
                        <span class="boton"><input class="submit" type="submit" name="devolver" value="Devolver"/></span>           
                </div>
                </form>                
                <!-- ------------------------------------------- -->
            <p>&nbsp;</p><p>&nbsp;</p>                     
            <div class="cl">&nbsp;</div><p>&nbsp;</p>            
            <a href="PrestamosOpciones.php"><img src="css/images/listas.png" width="50" height="50" title="Acerca de los Préstamos" alt="Acerca de los Préstamos"></a><legend>MÁS INFO</legend>
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>	