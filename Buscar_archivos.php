<?php
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/Entrada.php';
include_once 'clases/FuncionesABD.php';
//----- SE OBTIENEN TODOS LOS DATOS ENVIADOS POR EL FORMULARIO -----//
$idseccion = $_POST['secciones'];
$idmanzana = $_POST['manzanas'];
$lote= $_POST['lote'];
  $lote = strtoupper( $lote );
$noexpediente= $_POST['noexpediente'];
  $noexpediente = strtoupper( $noexpediente );
$idsituacionjur= $_POST['situacionjur'];
$idsituacionfis= $_POST['situacionfis'];
$franja= $_POST['franja'];

//SE ESTABLECEN LOS VALORES QUE SE ENVIARON
$entradaObj = new Entrada();
$entradaObj->setIdseccion($idseccion);
$entradaObj->setIdmanzana($idmanzana);
$entradaObj->setLote($lote);
$entradaObj->setNoexpediente($noexpediente);
$entradaObj->setIdsituacionjur($idsituacionjur);
$entradaObj->setIdsituacionfis($idsituacionfis);
$entradaObj->setFranja($franja);

//SE MANDA LLAMAR A LA FUNCION 
$objDal=new FuncionesABD();
$resultado=$objDal->Buscar_archivos(&$entradaObj);
  
//SI SE OBTUVIERON DATOS
if($resultado!=NULL ){
    session_start();
    $_SESSION['resultado_busquedaSeg'] = $resultado;
    //REDIRECCIONA A (BUSCARRES.PHP) Y MUESTRA LOS RESULTADOS OBTENIDOS
    header("Location: BuscarRes.php");
}else{
    //SE REDIRECCIONA A (BUSCAR.PHP) MENSAJE 0: "NO ENCONTRADO"
    header("Location: Buscar.php?mensaje=0");
}    
//--------------------------------------------------------------------//
?>