//FUNCIONES APOYADAS DE OTRAS FUNCIONES PARA MOSTRAR LAS CONSULTAS DE LOS COMBOS 
$(document).ready(function(){
    cargar_secciones();
    cargar_seccionestodas();
    cargar_situacionjur();
    cargar_situacionfis();
    cargar_tipouser();
    $("#seccion").change(function(){dependencia_manzanas();});
    $("#manzana").attr("disabled",true);
    $("#secciones").change(function(){dependencia_manzanastodas();});
    $("#manzanas").attr("disabled",true);
});
//FUNCION QUE CARGA LAS SECCIONES USANDO 
function cargar_secciones(){
    $.get("cargar-secciones.php", function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $('#seccion').append(resultado);
        }
    });		
}
//FUNCION QUE CARGA LAS MANZANAS EN FUNCION DE LAS SECCIONES
function dependencia_manzanas(){
    var code = $("#seccion").val();
    $.get("dependencia-manzanas.php", { code: code },
    function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $("#manzana").attr("disabled",false);
            document.getElementById("manzana").options.length=1;
            $('#manzana').append(resultado);
        }
    });   
}
//FUNCION QUE CARGA LA SITUACION JURIDICA
function cargar_situacionjur(){
    $.get("cargar-situacionjur.php", function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $('#situacionjur').append(resultado);
        }
    });		
}
//FUNCION QUE CARGA LA SITUACION FISICA
function cargar_situacionfis(){
    $.get("cargar-situacionfis.php", function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $('#situacionfis').append(resultado);
        }
    });		
}
function cargar_tipouser(){
    $.get("cargar-tipouser.php", function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $('#tipouser').append(resultado);
        }
    });		
}

//FUNCIONES QUE CARGA SECCIONES Y MANZANAS DE BUSQUEDA GENERAL
function cargar_seccionestodas(){
    $.get("cargar-secciones.php", function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $('#secciones').append(resultado);
        }
    });		
}
function dependencia_manzanastodas(){
    var code = $("#secciones").val();
    $.get("dependencia-manzanas.php", { code: code },
    function(resultado){
        if(resultado == false){
            alert("Error");
        }else{
            $("#manzanas").attr("disabled",false);
            document.getElementById("manzanas").options.length=1;
            $('#manzanas').append(resultado);
        }
    });   
}