$(function() { /* Efecto de la Barra */
    $("#barra ul").lavaLamp({
        fx: "backout", 
        speed: 700,
        click: function(event, menuItem) {
        	$('#barra li.current').removeClass('current');
        	$(this).addClass('current');
            return false;
        }
    });
		    
	$('#deslizador ul.deslizador-elementos').jcarousel({
		'scroll': 1,
		'auto': 10,
		'wrap': 'both',
        initCallback: mycarousel_initCallback,
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemVisibleInCallback: {
			onAfterAnimation: function(c, o, i, s) {
				jQuery('.deslizador-nav li').removeClass('active');
				jQuery('.deslizador-nav li:eq('+ (i-1) +')').addClass('active');
			}
		}
	});
	
	$('.fichero_enlacevi�t .boton').hover(function(){
		$(this).addClass('boton-act');
	}, function(){
		$(this).removeClass('boton-act');
	});
	
	if ($.browser.msie && $.browser.version == 6) {
		DD_belatedPNG.fix('#encabezado, h1#logo a, li.back, li.back .left, #deslizador, .marco, ul.deslizador-elementos li h2, .deslizador-nav, .nav-interior, #principal-arriba, #principal-abajo, #principal-contenido, .barra_sitio-exterior, .barra_sitio-abajo, .barra_sitio-contenido, .fichero_enlacevi�t h2, #deslizador .boton, #deslizador .boton span, .post-info, .separador');
	}
});
/*Deslizador Clip con Botones, efecto de Cambio de Botones*/
function mycarousel_initCallback(carousel) {
    $('.deslizador-nav .nav-interior').append('<ul></ul>');
    var i = 1;
    $('.deslizador-elementos li').each(function(){
    	$('.deslizador-nav ul').append('<li><a href="#">' + i + '</a></li>');
    	i++;
    });
    $('.deslizador-nav .nav-interior').append('<div class="cl">&nbsp;</div>');
    $('.deslizador-nav li:last').addClass('last');
    $('.deslizador-nav a').bind('click', function() {
        carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
        return false;
    });
	$('.deslizador-nav').css('margin-top', function(){
		return -($(this).find('.nav-interior').outerHeight() / 2 + 7) + 'px';
	});
};



