<?php
//----------- SE OBTIENEN VALORES ENVIADOS POR URL ------------// 
$valor=$_GET['valor']; //SE OBTIENE VALOR DE (IDTERRENO)
?>
<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'clases/FuncionesABD.php' ?>
<?php require 'config/Mysql.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <?php
                $obj = new FuncionesABD();
                $lista=$obj->Expediente_buscar_archivoesp($valor); 
                foreach($lista as $indice => $lista):
                $objUsuarioTmp = unserialize($lista);
                ?>
                <h2><a title="Expediente">EXPEDIENTE: <?php echo $objUsuarioTmp->getNoexpediente() ?></a></h2>
                <div class="cl">&nbsp;</div>              
            </div>
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div class="formulario" align="center"> 
                    <!-- --------- DATOS GENERALES ----------------- -->
                    <fieldset><legend></legend><p>&nbsp;</p>                          
                        <div><hr align="center" width="400" size="2" /></div>
                        <legend>DATOS GENERALES</legend>                        
                        <div><hr align="center" width="400" size="2" /></div><p>&nbsp;</p>
                        <label>SECCI&Oacute;N: </label><input type="text" id="seccion" name="seccion" readonly="readonly" value="<?php echo $objUsuarioTmp->getIdseccion()?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>MANZANA: </label><input type="text" id="manzana" name="manzana" readonly="readonly" value="<?php echo $objUsuarioTmp->getIdmanzana()?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>LOTE: </label><input type="text" id="lote" name="lote" readonly="readonly" value="<?php echo $objUsuarioTmp->getLote()?>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>NO. EXPEDIENTE: </label><input type="text" id="noexpediente" name="noexpediente" readonly="readonly" value="<?php echo $objUsuarioTmp->getNoexpediente()?>"/><p>&nbsp;</p> 
                        <div>                            
                            <label>SITUACI&Oacute;N JUR&Iacute;DICA: </label><input type="text" id="sitjur" name="sitjur" readonly="readonly" value="<?php echo $objUsuarioTmp->getIdsituacionjur()?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                     
                            <label>SITUACI&Oacute;N F&Iacute;SICA: </label><input type="text" id="sitfis" name="sitfis" readonly="readonly" value="<?php echo $objUsuarioTmp->getIdsituacionfis()?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                             
                            <label>FRANJA: </label><input type="text" id="franja" readonly="readonly" name="franja" value="<?php echo $objUsuarioTmp->getFranja()?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                             
                        </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                        <!-- ------------ SUPERFICIES ------------------ -->
                        <div>
                        <div><hr align="center" width="400" size="2" /></div>
                        <legend>SUPERFICIES</legend>                        
                        <div><hr align="center" width="400" size="2" /></div><p>&nbsp;</p>
                        <table>                            
                            <tr><td style="background: #035fad ;" rowspan="3">SUPERFICIE</td><td style="background: #2683d1 ;">REGULARIZADA: </td><td class="azul"><input type="text" name="regularizada" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>
                            <tr>                                                             <td style="background: #2683d1;">DESLINDADA: </td><td class="azul"><input type="text" name="deslindada" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #2683d1;">EXTINTA: </td><td class="azul"><input type="text" name="extinta" class="validate[custom[number]] text-input"/></td></tr>
                            <tr><td style="background: #035fad;" rowspan="2">EXCEDENTE</td><td style="background: #2683d1;">ZONA FEDERAL LACUSTRE: </td><td class="azul"><input type="text" name="zonafedlac" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>
                            <tr>                                                           <td style="background: #2683d1;">FILATEQ: </td><td class="azul"><input type="text" name="filateq" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>                            
                            <tr><td></td><td><hr style="float:right;" width="280" size="2" /></td><td><hr style="float:left;" width="150" size="2" /></td></tr>
                            <tr><td>Total</td><td><label>SUPERFICIE F&Iacute;SICA:</label></td><td class="azul"><input type="text" name="fisica" readonly="readonly" /><label> M<sup>2</sup></label></td></tr>
                       </table>
                       </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                       <!-- ------------- PROPIETARIO ---------------- -->
                       <div>
                        <div><hr align="center" width="400" size="2" /></div>
                        <legend>PROPIETARIO</legend>                        
                        <div><hr align="center" width="400" size="2" /></div><p>&nbsp;</p>
                       </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                       <!-- ---------- ARCHIVOS E IMAGENES ----------------- -->
                       <div>
                        <div><hr align="center" width="400" size="2" /></div>
                        <legend>ARCHIVOS E IMAGENES</legend>                        
                        <div><hr align="center" width="400" size="2" /></div><p>&nbsp;</p>                         
                        <?php echo $objUsuarioTmp->getIdarchivo()?>
                       </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                       <!-- ----------- OBSERVACIONES ----------------- -->
                       <div>
                        <div><hr align="center" width="400" size="2" /></div>
                        <legend>OBSERVACIONES</legend>                        
                        <div><hr align="center" width="400" size="2" /></div><p>&nbsp;</p>
                        <textarea id="observaciones" name="observaciones" cols="90" rows="5" readonly="readonly" ><?php echo $objUsuarioTmp->getObservaciones()?></textarea><p>&nbsp;</p>
                       </div> 
                    </fieldset><p>&nbsp;</p>
                    <!-- ----------- OPCIONES ------------ -->
                    <div>
                        <table class="centrar">
                            <tr>
                                <td><a href="BuscarRes.php"><img src="css/images/regresar.png" title="Regresar" /></a></td>
                                <td><a><img src="css/images/editar.png" title="Editar"></a></td>
                                <td><a><img src="css/images/imprimir.png" title="Imprimir" /></a></td>
                                <td><a><img src="css/images/guardar.png" title="Guardar" /></a></td>
                            </tr>
                            <tr>
                                <td><a href="BuscarRes.php"><legend>REGRESAR  |</legend></a></td>
                                <td><a href="ExpedienteModificar.php?valor=<?php echo $objUsuarioTmp->getIdterreno(); ?>" ><legend>|  EDITAR  |</legend></a></td>
                                <td><a><legend>|  IMPRIMIR  |</legend></a></td>
                                <td><a><legend>|  GUARDAR</legend></a></td>
                            </tr>
                        </table>
                    </div>
                    <!-- ------------------------------------------- -->                
                   <div class="cl">&nbsp;</div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>                  
                   <?php endforeach;?>          
            </div>
            <div class="cl">&nbsp;</div>
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Inicio Contenido -->
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>