<!-- ----------------VERIFIICA SI ESTA LOGEADO-------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1");
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?>
<?php $fechaActual=  date("d/m/Y");?>
<!-- ------------------------------------------------------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- --VERIFICA SI SE GUARDARON LOS DATOS PARA MANDAR MSJ-- -->
<?php
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {
        case 1:
            $mensaje1="EL PRESTAMO HA SIDO REGISTRADO EXITOSAMENTE !!!";
            break;
        case 0:
            $mensaje0="NO SE ENCUENTRA DISPONIBLE";
            
            break;
        default:
            break;
    }
} 

if(isset($mensaje0)){
  echo "<div class='alerta mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
if(isset($mensaje1)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2><a title="Buscar">PRÉSTAMO REALIZADO</a></h2>
                <div class="cl">&nbsp;</div>              
            </div>            
            <!-- Inicio Contenido -->
            <p class="post-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong><?php echo $fechaActual ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?> </p>
            <div class="formulario" align="center">
            <!-- ------------------------------------------- -->
                 <?php if(isset($_SESSION['resultado_prestamo'])):?>
                <?php $result = $_SESSION['resultado_prestamo'];
                        foreach($result as $indice => $objUsuario):
                            $objUsuarioTmp = new Entrada();
                            $objUsuarioTmp = unserialize($objUsuario);?>
                    <table class="tabla2" width="50%">
                        <th style="background: #2683d1;" colspan="9"><h3>NO. TICKET: <?php echo $objUsuarioTmp->getTicket()?></h3></th>
                        <tr></tr><tr></tr><tr></tr>                        
                        <tr>
                            <th>USUARIO</th><td><?php echo $objUsuarioTmp->getNombre()?>&nbsp;<?php echo $objUsuarioTmp->getAp()?></td>
                        </tr>
                        <tr>
                            <th>NO. EXPEDIENTE</th><td><?php echo $objUsuarioTmp->getNoexpediente()?></td>
                        </tr>
                        <tr>
                            <th>FECHA DE PRÉSTAMO</th><td><?php echo $objUsuarioTmp->getPrestamo()?></td>
                        </tr>
                        <tr>    
                            <th>FECHA DE DEVOLUCIÓN</th> <td><?php echo $objUsuarioTmp->getDevolucion()?></td>                                                                                 
                        </tr>
                        <?php endforeach;
                        unset($_SESSION['resultado_prestamo']); ?>
                    </table>                    
                    <?php endif; ?>   
                <!-- ----------- OPCIONES ------------ -->
                <div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                    <table class="centrar">
                        <tr>
                            <td><a href="Prestamos.php"><img src="css/images/regresar.png" title="Regresar"></a></td>
                            <td><img src="css/images/imprimir.png" title="Imprimir"></td>
                            <td><img src="css/images/guardar.png" title="Guardar"></td>
                        </tr>
                        <tr>
                            <td><a href="Prestamos.php"><legend>REGRESAR  |</legend></a></td>
                            <td><a><legend>|  IMPRIMIR  |</legend></a></td>
                            <td><a><legend>|  GUARDAR</legend></a></td>
                        </tr>
                    </table>
                </div>
                <!-- --------------------------------- -->
             </div><p>&nbsp;</p> 
            <div class="cl">&nbsp;</div><p>&nbsp;</p>          
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>	