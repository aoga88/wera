<!-- --------------- VERIFIICA SI ESTA LOGEADO ------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2><a title="Buscar">RESULTADOS</a></h2>
                <div class="cl">&nbsp;</div>              
            </div>
            <!-- Inicio Contenido -->
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div><p>&nbsp;&nbsp;</p></div>
            <div class="formulario" align="center">                                        
                <!-- ------------- RESULTADOS BUSQESP ------------- -->
                <div class="centrar">        
                    <?php if(isset($_SESSION['resultado_busqueda'])):?>
                    <table class="tabla2" width="100%">
                        <tr>
                            <th>NO.</th>
                            <th>SECCION</th>
                            <th>MANZANA</th>
                            <th>LOTE</th>
                            <th>NO.EXPEDIENTE</th>
                            <th>SITUACIÓN JURÍDICA</th>
                            <th>SITUACIÓN FÍSICA</th>
                            <th>FRANJA</th>                            
                            <th>DETALLES<th>                        
                        <?php $result = $_SESSION['resultado_busqueda'];
                        foreach($result as $indice => $objUsuario):
                            $objUsuarioTmp = new Entrada();
                            $objUsuarioTmp = unserialize($objUsuario);?>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getFila()?></td>
                            <td><?php echo $objUsuarioTmp->getIdseccion()?></td>
                            <td><?php echo $objUsuarioTmp->getIdmanzana()?></td>
                            <td><?php echo $objUsuarioTmp->getLote()?></td>
                            <td><?php echo $objUsuarioTmp->getNoexpediente()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionjur()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionfis()?></td>
                            <td><?php echo $objUsuarioTmp->getFranja()?></td>
                            <td><a href="Expediente.php?valor=<?php echo $objUsuarioTmp->getIdterreno(); ?>" ><img style="float:right;" src="css/images/mas.png" title="Más Información" alt="+ INFO >>>"/></a></td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td style="background: #ffffff; color: #ffffff; ">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>TOTAL</th>                            
                        </tr>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getTotal() ?></td>
                        </tr>                        
                    </table>                    
                    <?php endif; ?>                                       
                </div>
                <!-- ------------- RESULTADOS BUSQGRAL ------------- -->
                <div class="centrar">               
                    <?php if(isset($_SESSION['resultado_busquedaSeg'])):?>
                   <table class="tabla2" width="100%">
                        <tr>
                            <th>NO.</th>
                            <th>SECCION</th>
                            <th>MANZANA</th>
                            <th>LOTE</th>
                            <th>NO.EXPEDIENTE</th>
                            <th>SITUACIÓN JURÍDICA</th>
                            <th>SITUACIÓN FÍSICA</th>
                            <th>FRANJA</th>                            
                            <th>DETALLES<th>                        
                        <?php $result = $_SESSION['resultado_busquedaSeg'];
                        foreach($result as $indice => $objUsuario):
                            $objUsuarioTmp = new Entrada();
                            $objUsuarioTmp = unserialize($objUsuario);?>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getFila()?></td>
                            <td><?php echo $objUsuarioTmp->getIdseccion()?></td>
                            <td><?php echo $objUsuarioTmp->getIdmanzana()?></td>
                            <td><?php echo $objUsuarioTmp->getLote()?></td>
                            <td><?php echo $objUsuarioTmp->getNoexpediente()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionjur()?></td>
                            <td><?php echo $objUsuarioTmp->getIdsituacionfis()?></td>
                            <td><?php echo $objUsuarioTmp->getFranja()?></td>
                            <td><a href="Expediente.php?valor=<?php echo $objUsuarioTmp->getIdterreno(); ?>" ><img style="float:right;" src="css/images/mas.png" title="Más Información" alt="+ INFO >>>"/></a></td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td style="background: #ffffff; color: #ffffff; ">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>TOTAL</th>                            
                        </tr>
                        <tr>
                            <td><?php echo $objUsuarioTmp->getTotal() ?></td>
                        </tr>                        
                    </table> 
                    <?php endif; ?>                     
                </div>  
                <!-- ----------- OPCIONES ------------ -->
                <div>
                    <table class="centrar">
                        <tr>
                            <td><a href="Buscar.php"><img src="css/images/regresar.png" title="Regresar" /></a></td>
                            <td><a><img src="css/images/imprimir.png" title="Imprimir" /></a></td>
                            <td><a><img src="css/images/guardar.png" title="Guardar" /></a></td>
                        </tr>
                        <tr>
                            <td><a href="Buscar.php"><legend>REGRESAR  |</legend></a></td>
                            <td><a><legend>|  IMPRIMIR  |</legend></a></td>
                            <td><a><legend>|  GUARDAR</legend></a></td>
                        </tr>
                    </table>
                </div>
                <!-- ------------------------------------------- -->
            </div><p>&nbsp;</p>
            <div class="cl">&nbsp;</div>           
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>