<?php
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/EntradaUser.php';
include_once 'clases/FuncionesABD.php';
//---- REVISA QUE EXISTAN LOS VALORES ENVIADOS POR EL FORMULARIO ----//
if (isset($_POST['id']) && $_POST['tipouser'] && $_POST['nombre'] && $_POST['ap'] && $_POST['am'] && $_POST['cargo'] && $_POST['usuario'] && $_POST['password']){
    //SE OBTIENEN TODOS LOS DATOS ENVIADOS POR EL FORMULARIO
    $id=$_POST['id'];
    $idtipousuario= $_POST['tipouser'];
    $nombre= $_POST['nombre'];
      $nombre = strtoupper( $nombre );
    $ap= $_POST['ap'];
      $ap = strtoupper( $ap );
    $am= $_POST['am'];
      $am = strtoupper( $am );
    $cargo= $_POST['cargo'];
      $cargo = strtoupper( $cargo );
    $usuario= $_POST['usuario'];
    $password= $_POST['password'];

    //SE ESTABLECEN LOS VALORES QUE SE ENVIARON
    $entradaObjUser = new EntradaUser();
    $entradaObjUser->setIdusuario($id);
    $entradaObjUser->setIdtipousuario($idtipousuario);
    $entradaObjUser->setNombre($nombre);
    $entradaObjUser->setApellidop($ap);
    $entradaObjUser->setApellidom($am);
    $entradaObjUser->setCargo($cargo);
    $entradaObjUser->setUsuario($usuario);
    $entradaObjUser->setPassword($password);

    //SE MANDA LLAMAR A LA FUNCION
    $entradaDALObjUser = new FuncionesABD();
    $resultado=$entradaDALObjUser->Permisos_guardarmodif($entradaObjUser);
    //REDIRECCIONA A (PERMISOS.PHP) MENSAJE 3: "SE HA MODIFICADO CON EXITO"
    header("Location: Permisos.php?mensaje=3");
}else{
    //SE REDIRECCIONA A (PERMISOS.PHP) MENSAJE 2: "ERROR AL MODIFICARSE INTENTELO NUEVAMENTE"
    header("Location: Permisos.php?mensaje=2");
}
//--------------------------------------------------------------------//   
?>