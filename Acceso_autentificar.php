<?php
//------------------------ INICIA LA SESION --------------------------//
session_start();
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/Usuarios.php';
include_once 'clases/FuncionesABD.php';
//---- REVISA QUE EXISTAN LOS VALORES ENVIADOS POR EL FORMULARIO ----//
if (isset($_POST['usuario']) && isset($_POST['password'])){
    $objDal=new FuncionesABD();
    $usuario=mysql_real_escape_string($_POST['usuario']);
$password=mysql_real_escape_string($_POST['password']);
    $objUsuario=$objDal->Acceso_autentificar($usuario, sha1($password));
    
    //SI EL QUE SE AUTENTIFICO ES ADMINISTRADOR
    if($objUsuario!=NULL && $objUsuario->tipo_usuario=='ADMINISTRADOR'){
        $_SESSION['admin_session']='SI';
        $_SESSION['admin_nombre']=$objUsuario->getNombre();
        $_SESSION['admin_usuario']=$objUsuario->getUsuario();
        $_SESSION['tipo_usuario']=$objUsuario->tipo_usuario;
        header("Location: Inicio.php");
     //SI EL QUE SE AUTENTIFICO ES USUARIO    
    }else if($objUsuario!=NULL && $objUsuario->tipo_usuario=='USUARIO'){
        $_SESSION['admin_session']='SI';
        $_SESSION['admin_nombre']=$objUsuario->getNombre();
        $_SESSION['admin_usuario']=$objUsuario->getUsuario();
        $_SESSION['tipo_usuario']=$objUsuario->tipo_usuario;
        header("Location: Inicio.php");
    }else{
        //NO SE LOGEA NADIE, MANDA MENSAJE (ACCESO:PHP) MENSAJE 0 : "USUARIO O PASSWORD INCORRECTOS"
        header("Location: Acceso.php?mensaje=0");
    }    
}
//--------------------------------------------------------------------//
?>