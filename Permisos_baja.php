<?php
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/EntradaUser.php';
include_once 'clases/FuncionesABD.php';
//----------- SE OBTIENEN VALORES ENVIADOS POR URL ------------// 
if (isset($_GET['valor'])){
$valor=$_GET['valor']; //SE OBTIENE VALOR DE (IDUSUARIO)

//SE MANDA LLAMAR A LA FUNCION
$entradaDALObjUser = new FuncionesABD();
$resultado=$entradaDALObjUser->Permisos_baja($valor);
//REDIRECCIONA A (PERMISOS.PHP) MENSAJE 5: "SE HA ELIMINADO CON EXITO"
header("Location: Permisos.php?mensaje=5");
}else{
    //SE REDIRECCIONA A (PERMISOS.PHP) MENSAJE 4: "ERROR AL ELIMINARSE INTENTELO NUEVAMENTE"
    header("Location: Permisos.php?mensaje=4");
}
//--------------------------------------------------------------------//    
?>