<?php
//----------- SE OBTIENEN VALORES ENVIADOS POR URL ------------// 
$valor=$_GET['valor']; //SE OBTIENE VALOR DE (IDUSUARIO)
?>
<!-- ----------------VERIFIICA SI ESTA LOGEADO-------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/EntradaUser.php' ?>
<?php require 'clases/FuncionesABD.php' ?>
<?php require 'config/Mysql.php' ?>
<?php require 'includes/EncabezadoValOrig.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <!-- Inicio Barra_sitio -->
        <div id="barra_sitio" float="right">
            <div class="barra_sitio-exterior">
                <div class="barra_sitio-contenido">
                    <ul>
                        <li class="fichero_enlacevi�t">
                            <div class="titulo">
                                <h2><a>MODIFICAR USUARIO</a></h2>                                                                
                            <div class="cl">&nbsp;</div>              
                            </div>&nbsp;                         
                            <form name="formNuevoUser" action="Permisos_guardarmodif.php" method="POST" id="formNuevoUser" accept-charset="utf-8">
                                <div class="centrar"><img src="css/images/edituser.png" /></div>&nbsp; 
                                <label>TIPO DE USUARIO: </label>
                                <select id="tipouser" name="tipouser" class="validate[required]">   
                                <option value=" ">SELECCIONAR...</option>                                
                                </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php
                                $obj = new FuncionesABD();
                                $lista=$obj->PermisosModificar_buscar_usuario($valor); 
                                foreach($lista as $indice => $lista):
                                $objUsuarioTmp = unserialize($lista);
                                ?> 
                                <div>&nbsp;<input type="hidden" id="id" name="id" value="<?php echo $objUsuarioTmp->getIdusuario()?>"/></div>
                                <div class="largo">&nbsp;<label>NOMBRE:</label><input type="text" id="nombre" name="nombre" class="validate[required,custom[onlyLetterSp]] text-input" value="<?php echo $objUsuarioTmp->getNombre()?>"/></div>
                                <div class="largo">&nbsp;<label>APELLIDO PATERNO:</label><input type="text" id="ap" name="ap" class="validate[required,custom[onlyLetterSp]] text-input" value="<?php echo $objUsuarioTmp->getApellidop()?>"/></div>
                                <div class="largo">&nbsp;<label>APELLIDO MATERNO:</label><input type="text" id="am" name="am" class="validate[required,custom[onlyLetterSp]] text-input" value="<?php echo $objUsuarioTmp->getApellidom()?>"/></div>
                                <div class="largo">&nbsp;<label>CARGO:</label><input type="text" id="cargo" name="cargo" class="validate[required,custom[onlyLetterSp]] text-input" value="<?php echo $objUsuarioTmp->getCargo()?>"/></div>
                                <div>&nbsp;<label>USUARIO:</label><input type="text" id="usuario" name="usuario" class="validate[required,custom[onlyLetterNumber]] text-input" value="<?php echo $objUsuarioTmp->getUsuario()?>"/></div>
                                <div>&nbsp;<label>CONTRASE&Ntilde;A:</label><input type="password" id="password" name="password" class="validate[required] text-input" /></div>
                                <div>&nbsp;<label>REPETIR CONTRASE&Ntilde;A:</label><input type="password" id="password2" name="password2" class="validate[required,equals[password]] text-input" /></div>
                                <span class="boton"><input class="submit" type="submit" value="Modificar" /></span>
                              <?php endforeach;?>     
                            </form>                                                     
                        </li>
                    </ul>
                    <div class="cl">&nbsp;</div>
                </div>
            </div>
            <div class="barra_sitio-abajo">&nbsp;</div>
        </div>
        <!-- Fin Barra de Sitio -->
        <!-- Inicio Contenido -->
        <div id="contenido">                    
            <div class="post">
                <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
                <div class="formulario" align="center"> 
                <div>                    
                    <table class="otro" width="80%">
                        <th style="background: #2683d1;" colspan="9"><h3>USUARIO</h3></th>
                        <tr></tr><tr></tr><tr></tr>
                            <tr>                           
                                <th>NOMBRE</th><td><?php echo $objUsuarioTmp->getNombre()?></td>
                            </tr>
                            <tr>
                                <th>APELLIDO PATERNO.</th><td><?php echo $objUsuarioTmp->getApellidop()?></td>
                            </tr>
                            <tr>
                                <th>APELLIDO MATERNO</th><td><?php echo $objUsuarioTmp->getApellidom()?></td>
                            </tr>
                            <tr>
                                <th>CARGO</th><td><?php echo $objUsuarioTmp->getCargo()?> </td>
                            </tr>    
                            <tr>
                                <th>TIPO DE USUARIO</th><td><?php echo $objUsuarioTmp->getIdtipousuario()?></td>
                            </tr>    
                            <tr>
                                <th>USUARIO</th><td><?php echo $objUsuarioTmp->getUsuario()?></td>
                            </tr>                                                             
                    </table> 
                </div>
                <!-- ----------- OPCIONES ------------ -->
                <p>&nbsp;</p><p>&nbsp;</p>
                <div>
                    <table class="centrar">
                        <tr>
                            <td><a href="Permisos.php"><img src="css/images/regresar.png" title="Regresar" /></a></td>
                        </tr>
                        <tr>
                            <td><a href="Permisos.php"><legend>REGRESAR</legend></a></td>
                        </tr>
                    </table>
                </div>
                <!-- ------------------------------------------- -->
                </div>
                <div class="cl">&nbsp;</div>
                <div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>                             
                <p>&nbsp;</p><a href="#" class="mas" title="Salir"><span class="separador">&nbsp;</span><span onclick="location='cerrarsesion.php'">SALIR</span></a>
                <div class="cl">&nbsp;</div>                
            </div> 
        </div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div> 
<!--Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>