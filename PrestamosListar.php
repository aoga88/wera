<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'clases/EntradaUser.php' ?>
<?php require 'clases/FuncionesABD.php' ?>
<?php require 'config/Mysql.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2><a title="Buscar">PRÉSTAMOS</a></h2>
                <div class="cl">&nbsp;</div>              
            </div>
            <!-- Inicio Contenido -->
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div class="formulario" align="center">  
                <div class="centrar">
                    <table class="tabla2" width="100%"> 
                        <?php
                        $obj = new FuncionesABD();
                        $lista=$obj->Prestamos_listar(); 
                        if ($lista==NULL){
                            $mensaje0="NO EXISTE NINGUN REGISTRO";
                            echo "<div class='alerta mensajes'>";
                            echo "$mensaje0";
                            echo "</div>";
                        }else{?>                        
                        <tr>
                            <th>NO.</th>
                            <th>TICKET</th>
                            <th>USUARIO</th>
                            <th>TERRENO NO. EXP</th>
                            <th>FECHA DE PRÉSTAMO</th>
                            <th>FECHA DE DEVOLUCIÓN</th>  
                            <th>¿DEVUELTO?</th>
                            <? foreach($lista as $indice => $lista):
                            $objUsuarioTmp = unserialize($lista);
                            ?>
                        <tr>                         
                            <td><?php echo $objUsuarioTmp->getFila() ?></td>
                            <td><?php echo $objUsuarioTmp->getTicket() ?></td>
                            <td><?php echo $objUsuarioTmp->getNombre() ?> <?php echo $objUsuarioTmp->getap() ?></td>
                            <td><?php echo $objUsuarioTmp->getNoexpediente() ?></td>
                            <td><?php echo $objUsuarioTmp->getPrestamo() ?></td>
                            <td><?php echo $objUsuarioTmp->getDevolucion() ?></td>
                            <td><?php echo $objUsuarioTmp->getEstado() ?></td>
                        </tr>                       
                        <?php endforeach; ?>
                        <tr>
                            <td style="background: #ffffff; color: #ffffff; ">&nbsp;</td>
                        </tr>
                        <tr>
                            <th>TOTAL</th>                            
                        </tr>
                        <tr> 
                            <td><?php echo $objUsuarioTmp->getTotal() ?> <?php }?></td> 
                        </tr>                        
                    </table>
                </div> 
                <!-- ----------- OPCIONES ------------ -->
                <div>
                    <table class="centrar">
                        <tr>
                            <td><a href="PrestamosOpciones.php"><img src="css/images/regresar.png" title="Regresar"></a></td>
                            <td><img src="css/images/imprimir.png" title="Imprimir"></td>
                            <td><img src="css/images/guardar.png" title="Guardar"></td>
                        </tr>
                        <tr>
                            <td><a href="PrestamosOpciones.php"><legend>REGRESAR  |</legend></a></td>
                            <td><a><legend>|  IMPRIMIR  |</legend></a></td>
                            <td><a><legend>|  GUARDAR</legend></a></td>
                        </tr>
                    </table>
                </div>
                <!-- --------------------------------- -->
                </div><p>&nbsp;</p> 
            <div class="cl">&nbsp;</div><p>&nbsp;</p>
            <!--<a href="Prestamos.php"><img src="css/images/volver.png" width="50" height="50"></a><p>&nbsp;</p>-->
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>			