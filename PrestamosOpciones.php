<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/Entrada.php' ?>
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2>PRÉSTAMOS</h2>
                <div class="cl">&nbsp;</div>              
            </div>            
            <!-- Inicio Contenido -->
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div><p>&nbsp;&nbsp;</p></div>
            <div class="formulario" align="center">
                <!-- ---------- OPCION PRÉSTAMOS O DEVOLUCIONES --------------- -->
                <div><p>&nbsp;</p></div>         
                <div id="formMasInfoPrestamos">
                    <table class="centrar">
                        <tr>
                            <td><a href="PrestamosListar.php"><img src="css/images/lista.png" width="50" height="50" title="Listar Préstamos" alt="Listar Préstamos"></a></td>
                            <td><img src="css/images/folder2.png" title="Imprimir"></td>
                        </tr>
                        <tr>
                            <td><a href="PrestamosListar.php"><legend>TOTAL DE PRÉSTAMOS   |</legend></a></td>
                            <td><a><legend>|   PRÉSTAMOS PERSONALES</legend></a></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- --------------------------------- -->
            <div class="cl">&nbsp;</div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                <div>
                    <table class="centrar">
                        <tr>
                            <td><a href="Prestamos.php"><img src="css/images/regresar.png" title="Regresar"></a></td>
                        </tr>
                        <tr>
                            <td><a href="Prestamos.php"><legend>REGRESAR </legend></a></td>
                        </tr>
                    </table>
                </div>
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>        
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>	