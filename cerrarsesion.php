<?php
//--------------------------------------------------------------------//
// INICIAMOS LA SESION
session_start();
//--------------------------------------------------------------------//
// DESTRUIR TODAS LAS VARIABLES DE SESION
$_SESSION = array();
//--------------------------------------------------------------------//
// NOTA: ¡Esto destruirá la sesión, y no la información de la sesión!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// FINALMENTE DESTRUIR LA SESION
session_destroy();
header("Location: Acceso.php");
//--------------------------------------------------------------------//
?>