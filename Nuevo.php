<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- - VERIFICA SI SE GUARDARON LOS DATOS PARA MANDAR MSJ - -->
<?php
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {
        case 0:
            $mensaje0="ERROR AL ALMACENARSE...INTENTELO NUEVAMENTE";
            break;
        case 1:
            $mensaje1="ALMACENADO EXITOSAMENTE !!!";
            break;  
        case 2:
            $mensaje2="ERROR DE IMAGEN...INTENTElO NUEVAMENTE";
            break; 
        default:
            break;
    }
}

if(isset($mensaje0)){
  echo "<div class='error mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
if(isset($mensaje1)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}
if(isset($mensaje2)){
  echo "<div class='error mensajes'>";
  echo "$mensaje2";
  echo "</div>";
}
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <div class="post">
            <div class="titulo">
                <h2>NUEVO</h2>
                <div class="cl">&nbsp;</div>              
            </div>
            <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
            <div class="formulario" align="center"> 
                <!-- ---------------- FORMNUEVO ---------------- -->
                <form name="formNuevo" id="formNuevo" class="formulario" action="Nuevo_crear.php" method="POST" enctype="multipart/form-data">
                    <!-- ---------- DATOS GENERALES --------------- -->
                    <fieldset><legend></legend><p>&nbsp;</p>
                        <div><hr align="center" width="800" size="2" /></div>
                        <legend>DATOS GENERALES</legend>
                        <div><hr align="center" width="800" size="2" /></div><p>&nbsp;</p>
                        <label>SECCI&Oacute;N: </label>
                        <select id="seccion" name="seccion" class="validate[required]">
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>MANZANA: </label>
                        <select id="manzana" name="manzana" class="validate[required]">
                        <option value=" ">SELECCIONAR...</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>LOTE: </label><input type="text" id="lote" name="lote" class="validate[required] text-input" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>NO. EXPEDIENTE: </label><input type="text" id="noexpediente" name="noexpediente" value="S/N" class="validate[required] text-input" /><p>&nbsp;</p> 
                        <div>                            
                            <label>SITUACI&Oacute;N JUR&Iacute;DICA: </label>                            
                            <select id="situacionjur" name="situacionjur" class="validate[required]"> 
                            <option value="">SELECCIONAR...</option>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>SITUACI&Oacute;N F&Iacute;SICA: </label> 
                            <select id="situacionfis" name="situacionfis" class="validate[required]"> 
                            <option value="">SELECCIONAR...</option>
                            </select>                                
                        </div><p>&nbsp;</p>
                    </fieldset><p>&nbsp;</p>
                    <!-- ------------- FRANJAS ----------------- -->
                    <fieldset><legend>&nbsp;FRANJA</legend><p>&nbsp;</p>
                        <label style="color: blue;">AZUL</label><input type="radio" id="franja0" name="franja" value="AZUL" onclick="MostrarFranja()" class="validate[required]" />
                        <label style="color: orange;">NARANJA</label><input type="radio" id="franja1" name="franja" value="NARANJA" onclick="MostrarFranja()" class="validate[required]" />
                        <label style="color: green;">VERDE</label><input type="radio" id="franja2" name="franja" value="VERDE" onclick="MostrarFranja()" class="validate[required]" /><p>&nbsp;</p>                                      
                    </fieldset><p>&nbsp;</p>
                    <!-- ------------- AZUL --------------------- -->
                    <div id="formAzul" style="display:none;">
                    <fieldset><legend>&nbsp;SUPERFICIES</legend><p>&nbsp;</p> 
                        <table>                            
                            <tr><td style="background: #035fad ;" rowspan="3">ACREDITADA</td><td style="background: #2683d1 ;">REGULARIZADA </td><td class="azul"><input type="text" name="aregularizada" id="aregularizada" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>
                            <tr>                                                             <td style="background: #2683d1;">DESLINDADA </td><td class="azul"><input type="text" name="adeslindada" id="adeslindada" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #2683d1;">EXTINTA </td><td class="azul"><input type="text" name="aextinta" id="aextinta" class="validate[custom[number]] text-input"/></td></tr>
                            <tr><td style="background: #035fad;" rowspan="2">EXCEDENTE</td><td style="background: #2683d1;">ZONA FEDERAL LACUSTRE </td><td class="azul"><input type="text" name="azonafedlac" id="azonafedlac" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>
                            <tr>                                                           <td style="background: #2683d1;">FILATEQ </td><td class="azul"><input type="text" name="aexcfilateq" id="aexcfilateq" class="validate[custom[number]] text-input" onChange="sumar()"/></td></tr>                            
                            <tr><td></td><td><hr style="float:right;" width="280" size="2" /></td><td><hr style="float:left;" width="150" size="2" /></td></tr>
                            <tr><td>Total</td><td><label>SUPERFICIE F&Iacute;SICA:</label></td><td class="azul"><input type="text" name="afisica" id="afisica" readonly="readonly" /><label> M<sup>2</sup></label></td></tr>
                       </table> 
                    </fieldset><p>&nbsp;</p>
                    </div>
                    <!-- ------------ NARANJA -------------------- -->
                    <div id="formNaranja" style="display:none;">
                    <fieldset><legend>&nbsp;SUPERFICIES</legend><p>&nbsp;</p> 
                        <table>
                            <tr><td style="background: #ff6633 ;" rowspan="5">SUPERFICIE</td><td style="background: #ff9933 ;">REGULARIZADA </td><td class="naranja"><input type="text" name="nregularizada" id="nregularizada"  class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #ff9933 ;">DESLINDADA </td><td class="naranja"><input type="text" name="ndeslindada" id="ndeslindada" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #ff9933 ;">EXTINTA </td><td class="naranja"><input type="text" name="nextinta" id="nextinta" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #ff9933 ;">ACREDITADA </td><td class="naranja"><input type="text" name="nacreditada" id="nacreditada" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                             <td style="background: #ff9933 ;">FILATEQ </td><td class="naranja"><input type="text" name="nfilateq" id="nfilateq"class="validate[custom[number]] text-input"/></td></tr>
                            <tr><td style="background: #ff6633;" rowspan="2">EXCEDENTE</td><td style="background: #ff9933;">ZONA FEDERAL </td><td class="naranja"><input type="text" name="nzonafed" id="nzonafed" class="validate[custom[number]] text-input"/></td></tr>
                            <tr>                                                           <td style="background: #ff9933;">FILATEQ </td><td class="naranja"><input type="text" name="nexcfilateq" id="nexcfilateq" class="validate[custom[number]] text-input"/></td></tr>                                     
                            <tr><td></td><td><hr style="float:right;" width="280" size="2" /></td><td><hr style="float:left;" width="150" size="2" /></td></tr>
                            <tr><td>Total</td><td><label>SUPERFICIE F&Iacute;SICA:</label></td><td class="naranja"><input type="text" name="nfisica" id="nfisica" /><label> M<sup>2</sup></label></td></tr>
                        </table>
                   </fieldset><p>&nbsp;</p>
                   </div>
                   <!-- ------------- VERDE ------------------- -->
                   <div id="formVerde" style="display:none;">
                   <fieldset><legend>&nbsp;SUPERFICIES</legend><p>&nbsp;</p> 
                       <table>
                           <tr><td style="background: green ;" rowspan="5">SUPERFICIE</td><td style="background: #009900 ;">REGULARIZADA </td><td class="verde"><input type="text" name="vregularizada" id="vregularizada" class="validate[custom[number]] text-input"/></td></tr>
                           <tr>                                                             <td style="background: #009900 ;">DESLINDADA </td><td class="verde"><input type="text" name="vdeslindada" id="vdeslindada" class="validate[custom[number]] text-input"/></td></tr>
                           <tr>                                                             <td style="background: #009900 ;">EXTINTA </td><td class="verde"><input type="text" name="vextinta" id="vextinta" class="validate[custom[number]] text-input"/></td></tr>
                           <tr>                                                             <td style="background: #009900 ;">ACREDITADA </td><td class="verde"><input type="text" name="vacreditada" id="vacreditada" class="validate[custom[number]] text-input"/></td></tr>
                           <tr>                                                             <td style="background: #009900 ;">FILATEQ</td><td class="verde"><input type="text" name="vfilateq" id="vfilateq" class="validate[custom[number]] text-input"/></td></tr>
                           <tr><td style="background: green;" rowspan="2">EXCEDENTE</td><td style="background: #009900;">ZONA FEDERAL </td><td class="verde"><input type="text" name="vzonafed" id="vzonafed" class="validate[custom[number]] text-input"/></td></tr>
                           <tr>                                                           <td style="background: #009900;">FILATEQ </td><td class="verde"><input type="text" name="vexcfilateq" id="vexcfilateq" class="validate[custom[number]] text-input"/></td></tr>
                           <tr><td></td><td><hr style="float:right;" width="280" size="2" /></td><td><hr style="float:left;" width="150" size="2" /></td></tr>
                           <tr><td>Total</td><td><label>SUPERFICIE F&Iacute;SICA:</label></td><td class="verde"><input type="text" name="vfisica" id="vfisica" /><label> M<sup>2</sup></label></td></tr>
                       </table>
                   </fieldset><p>&nbsp;</p>
                   </div>
                   <!-- -------------- PROPIETARIOS ---------------- -->
                   <fieldset><legend>&nbsp;PROPIETARIO</legend><p>&nbsp;</p>
                       <label>PERSONA F&Iacute;SICA</label><input type="radio" id="propietario0" name="propietario" value="PERSONA F&Iacute;SICA" onclick="MostrarPropietarios()" class="validate[required]" />
                       <label>PERSONA MORAL</label><input type="radio" id="propietario1" name="propietario" value="PERSONA MORAL" onclick="MostrarPropietarios()" class="validate[required]" />
                       <label>PROPIEDAD DE FILATEQ</label><input type="radio" id="propietario2" name="propietario" value="FILATEQ" onclick="MostrarPropietarios()" class="validate[required]" /><p>&nbsp;</p>
                       <!-- ---------------- FISICA ------------- -->
                       <div id="formFisica" style="display:none;"><p>&nbsp;</p> 
                           <table id="tablaCompradores">
                               <tr class="otro">
                                   <td><label>NOMBRE </label></td>
                                   <td><label>APELLIDO PATERNO </label></td>  
                                   <td><label>APELLIDO MATERNO </label></td>
                                   <td><label>TEL&Eacute;FONO </label></td> 
                                   <td><label>C&Oacute;DIGO POSTAL </label></td>  
                                   <td><label>DIRECCI&Oacute;N </label></td>
                              </tr>
                           </table>
                           <img class="centrar" src="css/images/user_add48x.png" onClick="agregarUsuario()" title="Agregar Propietario"/>                         
                       </div><p>&nbsp;</p>
                       <!-- --------------- MORAL ----------------- -->
                       <div id="formMoral" style="display:none;"><p>&nbsp;</p>
                                <label>RAZ&Oacute;N SOCIAL: </label><input type="text" name="razonsocial" id="razonsocial"  class="validate[required]"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label>TEL&Eacute;FONO: </label><input type="text" name="telefono" id="telefono" class="validate[required,custom[phone],maxSize[13]] text-input"/><p>&nbsp;</p>
                                <div class="otro"> <label>DIRECCI&Oacute;N: </label><input type="text" name="direccion" id="direccion"  class="validate[required]"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                       </div>
                   </fieldset><p>&nbsp;</p>
                   <!-- --------- ARCHIVOS E IMAGENES --------------- -->
                   <fieldset><legend>&nbsp;ARCHIVOS E IMAGENES</legend><p>&nbsp;</p>                       
                       <table>
                           <tr>
                               <td rowspan="2"><img src="css/images/nuevodoc.png"></td>                           
                               <td><label>T&Iacute;TULO: </label></td>
                               <td><input type="text" id="titulo" name="titulo" class="validate[required]"/></td>    
                       </tr>
                           <tr>
                               <td><label >ARCHIVO: </label></td><td><input type="file" id="archivo" name="archivo" class="validate[required]" /></td> 
                           </tr>
                       </table>
                   </fieldset><p>&nbsp;</p>
                   <!-- ----------OBSERVACIONES-------------- -->
                   <fieldset><legend>&nbsp;OBSERVACIONES</legend><p>&nbsp;</p>
                       <textarea id="observaciones" name="observaciones" cols="100" rows="5" ></textarea><p>&nbsp;</p>
                   </fieldset><p>&nbsp;</p><p>&nbsp;</p>
                   <div class="cl">&nbsp;</div>
                   <span class="boton"><input class="submit" type="submit" value="Guardar"/></span>
                </form>
                <!-- ------------------------------------------- -->
            </div>
            <div class="cl">&nbsp;</div>
            <a href="cerrarsesion.php" class="mas"  title="Salir"><span class="separador">&nbsp;</span><span onclick="location='modulo/cerrarsesion.php'">SALIR </span></a>
            <div class="cl">&nbsp;</div>            
        </div>
        <!-- Inicio Contenido -->
        <div id="contenido"></div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>