<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA" 
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'clases/EntradaUser.php' ?>
<?php require 'clases/FuncionesABD.php' ?>
<?php require 'config/Mysql.php' ?>
<?php require 'includes/EncabezadoValOrig.php' ?>
<?php require 'includes/DeslizadorImg3.php' ?>
<!-- - VERIFICA SI SE GUARDARON LOS DATOS PARA MANDAR MSJ - -->
<?php
if(isset($_GET['mensaje'])){
    switch ($_GET['mensaje']) {       
        case 0:
            $mensaje0="ERROR AL REGISTRARSE...INTENTELO NUEVAMENTE";
            break;
        case 1:
            $mensaje1="SE HA REGISTRADO CON EXITO !!!";
            break;
        case 2:
            $mensaje2="ERROR AL MODIFICARSE...INTENTELO NUEVAMENTE";
            break;
        case 3:
            $mensaje3="SE HA MODIFICADO CON EXITO !!! ";
            break;
         case 4:
            $mensaje4="ERROR AL ELIMINARSE...INTENTELO NUEVAMENTE";
            break;
        case 5:
            $mensaje5="SE HA ELIMINADO CON EXITO !!! ";
            break;
        default:
            break;
    }
}

if(isset($mensaje0)){
  echo "<div class='error mensajes'>";
  echo "$mensaje0";
  echo "</div>";
}
if(isset($mensaje1)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje1";
  echo "</div>";
}
if(isset($mensaje2)){
  echo "<div class='error mensajes'>";
  echo "$mensaje2";
  echo "</div>";
}
if(isset($mensaje3)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje3";
  echo "</div>";
}
if(isset($mensaje4)){
  echo "<div class='error mensajes'>";
  echo "$mensaje4";
  echo "</div>";
}
if(isset($mensaje5)){
  echo "<div class='exito mensajes'>";
  echo "$mensaje5";
  echo "</div>";
}
?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <!-- Inicio Barra_sitio -->
        <div id="barra_sitio" float="right">
            <div class="barra_sitio-exterior">
                <div class="barra_sitio-contenido">
                    <ul>
                        <li class="fichero_enlacevi�t">
                            <div class="titulo">
                                <h2><a>AGREGAR USUARIO</a></h2>                                                                
                            <div class="cl">&nbsp;</div>              
                            </div>&nbsp; 
                            
                            <!-- ---------- FORMNUEVOUSER ------------------ -->
                            <form name="formNuevoUser" action="Permisos_alta.php" method="POST" id="formNuevoUser" accept-charset="utf-8">
                                <div class="centrar"> <img src="css/images/user_add.png" /></div>&nbsp; 
                                <label>TIPO DE USUARIO: </label>
                                <select id="tipouser" name="tipouser" class="validate[required]">
                                <option value=" ">SELECCIONAR...</option>
                                </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="largo">&nbsp;<label>NOMBRE:</label><input  type="text" id="nombre" name="nombre" class="validate[required,custom[onlyLetterSp]] text-input"/></div>
                                <div class="largo">&nbsp;<label>APELLIDO PATERNO:</label><input type="text" id="ap" name="ap" class="validate[required,custom[onlyLetterSp]] text-input"/></div>
                                <div class="largo">&nbsp;<label>APELLIDO MATERNO:</label><input type="text" id="am" name="am" class="validate[required,custom[onlyLetterSp]] text-input"/></div>
                                <div class="largo">&nbsp;<label>CARGO:</label><input type="text" id="cargo" name="cargo" class="validate[required,custom[onlyLetterSp]] text-input"/></div>
                               
                                <div>&nbsp;<label>USUARIO:</label><input type="text" id="usuario" name="usuario" class="validate[required,custom[onlyLetterNumber]] text-input"/></div>
                                <div>&nbsp;<label>CONTRASE&Ntilde;A:</label><input type="password" id="password" name="password" class="validate[required] text-input" /></div>
                                <div>&nbsp;<label>REPETIR CONTRASE&Ntilde;A:</label><input type="password" id="password2" name="password2" class="validate[required,equals[password]] text-input" /></div>
                                <span class="boton"><input class="submit" type="submit" value="Agregar" /></span>
                            </form> 
                            <!-- ------------------------------------------- -->
                        </li>
                    </ul>
                    <div class="cl">&nbsp;</div>
                </div>
            </div>
            <div class="barra_sitio-abajo">&nbsp;</div>
        </div>
        <!-- Fin Barra de Sitio -->
        <!-- Inicio Contenido -->
        <div id="contenido">                    
            <div class="post">
                <p class="post-info"><strong><?php echo $_SESSION['tipo_usuario'] ?>:</strong> <?php echo $_SESSION['admin_nombre'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;&nbsp;&nbsp;<strong>HORA:</strong><?php echo $hora->format("g:i a"); ?></p>
                <div class="formulario" align="center">
                <!-- --------- LISTA DE USUARIOS --------------- -->
                <div >  
                    <table class="otro" width="80%">                   
                        <?php
                        $obj = new FuncionesABD();
                        $lista=$obj->Permisos_consultar_usuarios(); 
                        foreach($lista as $indice => $lista):
                        $objUsuarioTmp = unserialize($lista);
                        ?>                    
                        <th style="background: #2683d1;" colspan="9"><h3>USUARIO: <?php echo $objUsuarioTmp->getNombre()?></h3></th>
                        <tr></tr><tr></tr><tr></tr>                        
                            <tr>
                                <th>APELLIDO PATERNO</th><td><?php echo $objUsuarioTmp->getApellidop()?></td> <th>CAMBIOS</th>
                            </tr>
                            <tr>
                                <th>APELLIDO MATERNO</th><td><?php echo $objUsuarioTmp->getApellidom()?></td><td rowspan="2"><a href="PermisosModificar.php?valor=<?php echo $objUsuarioTmp->getIdusuario(); ?>" ><img style="float:left;" src="css/images/user_edit.png" title="Modificar" alt="MODIFICAR"/></a></td>
                            </tr>
                            <tr>
                                <th>CARGO</th><td><?php echo $objUsuarioTmp->getCargo()?> </td>
                            </tr>    
                            <tr>
                                <th>TIPO DE USUARIO</th><td><?php echo $objUsuarioTmp->getIdtipousuario()?></td><td rowspan="2"><a href="Permisos_baja.php?valor=<?php echo $objUsuarioTmp->getIdusuario(); ?>" onclick="return confirm('¿ ESTA SEGURO DE ELIMINAR AL USUARIO <?php echo $objUsuarioTmp->getNombre(); ?> CON ID <?php echo $objUsuarioTmp->getIdusuario(); ?> ?');" ><img style="float:right;" src="css/images/user_delete.png" title="Eliminar" alt="ELIMINAR"/></a></td></td>
                            </tr>    
                            <tr>
                                <th>USUARIO</th><td><?php echo $objUsuarioTmp->getUsuario()?></td>
                            </tr> 
                            <tr>
                                <td style="background: #ffffff; color: #ffffff; ">&nbsp;</td>
                            </tr>   
                            <?php endforeach;?>                         
                    </table> 
                </div>
                <!-- ------------------------------------------- -->
                </div>                        
                <div class="cl">&nbsp;</div>
                <div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>
                <a href="#" class="mas" title="Salir"><span class="separador">&nbsp;</span><span onclick="location='cerrarsesion.php'">SALIR</span></a>
                <div class="cl">&nbsp;</div>                
            </div> 
        </div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>
</div> 
<!--Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>		