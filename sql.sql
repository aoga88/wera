SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `bdfilateq` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `bdfilateq`;

-- -----------------------------------------------------
-- Table `bdfilateq`.`tipo_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`tipo_usuario` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`tipo_usuario` (
  `idtipo_usuario` INT NOT NULL AUTO_INCREMENT ,
  `tipo` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idtipo_usuario`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`usuarios` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`usuarios` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT ,
  `idtipo_usuario` INT NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `apellidop` VARCHAR(50) NOT NULL ,
  `apellidom` VARCHAR(50) NOT NULL ,
  `cargo` VARCHAR(100) NOT NULL ,
  `usuario` VARCHAR(100) NOT NULL ,
  `password` VARCHAR(100) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_usuario`),
  INDEX `fk_usuarios_tipo_usuario` (`idtipo_usuario` ASC) ,
  CONSTRAINT `fk_usuarios_tipo_usuario`
    FOREIGN KEY (`idtipo_usuario` )
    REFERENCES `bdfilateq`.`tipo_usuario` (`idtipo_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`modulos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`modulos` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`modulos` (
  `id_modulo` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(50) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_modulo`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`usuario_modulo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`usuario_modulo` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`usuario_modulo` (
  `id_usuario` INT NOT NULL,
  `id_modulo` INT NOT NULL )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`secciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`secciones` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`secciones` (
  `id_seccion` INT NOT NULL AUTO_INCREMENT ,
  `nombresec` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`id_seccion`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`manzanas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`manzanas` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`manzanas` (
  `id_manzana` INT NOT NULL AUTO_INCREMENT ,
  `nombreman` VARCHAR(10) NOT NULL ,
  `id_seccion` INT NOT NULL,
  PRIMARY KEY (`id_manzana`), 
  KEY `id_seccion` (`id_seccion`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`situacionjuridica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`situacionjuridica` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`situacionjuridica` (
  `id_situacionjur` INT NOT NULL AUTO_INCREMENT ,
  `nombresitjur` VARCHAR(50) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_situacionjur`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`situacionfisica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`situacionfisica` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`situacionfisica` (
  `id_situacionfis` INT NOT NULL AUTO_INCREMENT ,
  `nombresitfis` VARCHAR(50) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_situacionfis`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`superficie_azul`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`superficie_azul` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`superficie_azul` (
  `id_superficieazul` INT NOT NULL AUTO_INCREMENT ,
  `acre_regularizada` FLOAT NOT NULL ,
  `acre_deslindada` FLOAT NOT NULL ,
  `acre_extinta` FLOAT NOT NULL ,
  `exc_zonafedlac` FLOAT NOT NULL ,
  `exc_filateq` FLOAT NOT NULL ,
  `supfisica` FLOAT NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_superficieazul`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`superficie_naranja`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`superficie_naranja` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`superficie_naranja` (
  `id_superficienaranja` INT NOT NULL AUTO_INCREMENT ,
  `regularizada` FLOAT NOT NULL ,
  `deslindada` FLOAT NOT NULL ,
  `extinta` FLOAT NOT NULL ,
  `acreditada` FLOAT NOT NULL ,
  `filateq` FLOAT NOT NULL ,
  `exc_zonafed` FLOAT NOT NULL ,  
  `exc_filateq` FLOAT NOT NULL ,
  `supfisica` FLOAT NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_superficienaranja`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`superficie_verde`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`superficie_verde` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`superficie_verde` (
  `id_superficieverde` INT NOT NULL AUTO_INCREMENT ,
  `regularizada` FLOAT NOT NULL ,
  `deslindada` FLOAT NOT NULL ,
  `extinta` FLOAT NOT NULL ,
  `acreditada` FLOAT NOT NULL ,
  `filateq` FLOAT NOT NULL ,
  `exc_zonafed` FLOAT NOT NULL ,  
  `exc_filateq` FLOAT NOT NULL ,
  `supfisica` FLOAT NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_superficieverde`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`superficies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`superficies` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`superficies` (
  `id_superficie` INT NOT NULL AUTO_INCREMENT ,
  `tabla` VARCHAR(100) NOT NULL ,
  `idtabla` INT NOT NULL ,
  PRIMARY KEY (`id_superficie`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`compradores_fisicos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`compradores_fisicos` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`compradores_fisicos` (
  `id_compradorfis` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(50) NOT NULL ,
  `apellidop` VARCHAR(50) NOT NULL ,
  `apellidom` VARCHAR(50) NOT NULL ,
  `telefono` INT(15) NOT NULL ,
  `cp` INT(5) NOT NULL ,
  `direccion` VARCHAR(255) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_compradorfis`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`compradores_morales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`compradores_morales` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`compradores_morales` (
  `id_compradormor` INT NOT NULL AUTO_INCREMENT ,
  `razonsocial` VARCHAR(255) NOT NULL ,
  `telefono` INT(15) NOT NULL ,
  `direccion` VARCHAR(255) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_compradormor`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`compradores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`compradores` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`compradores` (
  `id_comprador` INT NOT NULL AUTO_INCREMENT ,
  `tabla` VARCHAR(100) NOT NULL ,
  `idtabla` INT NOT NULL ,
  PRIMARY KEY (`id_comprador`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`terrenos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`terrenos` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`terrenos` (
  `id_terreno` INT NOT NULL AUTO_INCREMENT ,
  `id_seccion` INT NOT NULL,
  `id_manzana` INT NOT NULL,
  `lote` VARCHAR(50) NOT NULL,
  `noexpediente` VARCHAR(50) NOT NULL UNIQUE,
  `id_situacionjur` INT  NOT NULL ,
  `id_situacionfis` INT  NOT NULL ,
  `franja` VARCHAR(10)  NOT NULL ,
  `id_superficie` INT NOT NULL,    
  `id_comprador` INT NOT NULL,
  `id_archivo` INT NOT NULL,
  `observaciones` TEXT NULL ,  
  PRIMARY KEY (`id_terreno`) ,
  INDEX `fk_terrenos_secciones` (`id_seccion` ASC) ,
  INDEX `fk_terrenos_manzanas` (`id_manzana` ASC) ,
  INDEX `fk_terrenos_situacionjuridica` (`id_situacionjur` ASC) ,
  INDEX `fk_terrenos_situacionfisica` (`id_situacionfis` ASC) ,
  INDEX `fk_terrenos_superficies` (`id_superficie` ASC) ,
  INDEX `fk_terrenos_compradores` (`id_comprador` ASC) ,
  INDEX `fk_terrenos_archivos` (`id_archivo` ASC) ,
  CONSTRAINT `fk_terrenos_secciones`
    FOREIGN KEY (`id_seccion` )
    REFERENCES `bdfilateq`.`secciones` (`id_seccion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_manzanas`
    FOREIGN KEY (`id_manzana` )
    REFERENCES `bdfilateq`.`manzanas` (`id_manzana` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_situacionjuridica`
    FOREIGN KEY (`id_situacionjur` )
    REFERENCES `bdfilateq`.`situacionjuridica` (`id_situacionjur` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_situacionfisica`
    FOREIGN KEY (`id_situacionfis` )
    REFERENCES `bdfilateq`.`situacionfisica` (`id_situacionfis` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_superficies`
    FOREIGN KEY (`id_superficie` )
    REFERENCES `bdfilateq`.`superficies` (`id_superficie` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_compradores`
    FOREIGN KEY (`id_comprador` )
    REFERENCES `bdfilateq`.`compradores` (`id_comprador` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_terrenos_archivo`
    FOREIGN KEY (`id_archivo` )
    REFERENCES `bdfilateq`.`archivos` (`id_archivo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`prestamos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`prestamos` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`prestamos` (
  `idprestamo` INT NOT NULL AUTO_INCREMENT ,
  `no_ticket` VARCHAR(10) NOT NULL ,
  `id_usuario` INT NOT NULL ,
  `id_terreno` INT NOT NULL,
  `fechaprest` VARCHAR(45) NOT NULL ,
  `fechadevol` VARCHAR(45) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idprestamo`), 
  INDEX `fk_prestamos_usuarios` (`id_usuario` ASC) ,
  INDEX `fk_prestamos_terrenos` (`id_terreno` ASC) ,
  CONSTRAINT `fk_prestamos_usuarios`
    FOREIGN KEY (`id_usuario` )
    REFERENCES `bdfilateq`.`usuarios` (`id_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prestamos_terrenos`
    FOREIGN KEY (`id_terreno` )
    REFERENCES `bdfilateq`.`terrenos` (`id_terreno` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bdfilateq`.`archivos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdfilateq`.`archivos` ;
CREATE  TABLE IF NOT EXISTS `bdfilateq`.`archivos` (
  `id_archivo` INT NOT NULL AUTO_INCREMENT ,
  `titulo` VARCHAR(50) NOT NULL ,  
  `contenido` MEDIUMBLOB NOT NULL ,
  `tamanio` INTEGER UNSIGNED NULL,
  `tipo` VARCHAR(50) NOT NULL ,
  `nombre_archivo` VARCHAR(50) NOT NULL ,
  `tamanio_unidad` VARCHAR(50) NOT NULL ,
  `estado` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_archivo`) )
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bdfilateq`.`tipo_usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`tipo_usuario` (`idtipo_usuario`, `tipo`) VALUES (1, 'ADMINISTRADOR');
INSERT INTO `bdfilateq`.`tipo_usuario` (`idtipo_usuario`, `tipo`) VALUES (2, 'USUARIO');

COMMIT;
-- -----------------------------------------------------
-- Data for table `bdfilateq`.`tipo_usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`usuarios` (id_usuario, `idtipo_usuario`, `nombre`, `apellidop`, `apellidom`, `cargo`, `usuario`, `password`, `estado` ) VALUES (1, 1, 'KARLA', 'ALVARADO', 'MENDOZA', 'RESIDENTE', 'Karla','af1f674af0715d0b91787dfe5887af8eed39cb05',1);
INSERT INTO `bdfilateq`.`usuarios` (id_usuario, `idtipo_usuario`, `nombre`, `apellidop`, `apellidom`, `cargo`, `usuario`, `password`, `estado` ) VALUES (2, 2, 'USUARIO', 'USUARIO', 'USUARIO', 'USUARIO', 'Usuario','3f2ecdef3c6c3b614e34115a95b25944cfa4198a',1);
COMMIT;

-- -----------------------------------------------------
-- Data for table `bdfilateq`.`secciones`
-- -----------------------------------------------------

START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`secciones` (`id_seccion`, `nombresec`) VALUES 
    (1, 'PRIMERA'),
    (2, 'SEGUNDA'),
    (3, 'TERCERA'),
    (4, 'CUARTA'),
    (5, 'QUINTA'),
    (6, 'SEXTA');
COMMIT;

-- -----------------------------------------------------
-- Data for table `bdfilateq`.`manzanas`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`manzanas` (`id_manzana`, `nombreman`, `id_seccion`) VALUES 
    (1, '1', 1), (2, '2', 1), (3, '3', 1), (4, '4', 1), (5, '5', 1),
    (6, '6', 1), (7, '7', 1), (8, '8', 1), (9, '9', 1), (10, '10', 1),
    (11, '11', 1), (12, '12', 1), (13, '13', 1), (14, '14', 1), (15, '15', 1), 
    (16, '16', 1), (17, '17', 1), (18, '18', 1), (19, '19', 1), (20, '20', 1),
    (21, '21', 1), (22, '22', 1), (23, '23', 1), (24, '24', 1),
                                                                  
    (25, '1', 2), (26, '2', 2), (27, '3', 2), (28, '4', 2), (29, '5', 2), 
    (30, '6', 2), (31, '7', 2), (32, '8', 2), (33, '9', 2), (34, '10', 2), 
    (35, '11', 2), (36, '12', 2), (37, '13', 2), (38, '14', 2), (39, '15', 2), 
    (40, '16', 2), (41, '17', 2), (42, '18', 2), (43, '19', 2), (44, '20', 2), 
    (45, '21', 2), (46, '22', 2), (47, '23', 2), (48, '24', 2), (49, '25', 2), 
    (50, '26', 2), (51, '27', 2), (52, '28', 2), (53, '29', 2), (54, '30', 2), 
    (55, '31', 2), (56, '32', 2), 
    
    (57, '1', 3), (58, '2', 3), (59, '3', 3), (60, '4', 3), (61, '5', 3),
    (62, '6', 3), (63, '7', 3), (64, '8', 3), (65, '9', 3), (66, '10', 3), 
    (67, '11', 3), (68, '12', 3), (69, '13', 3), (70, '14', 3), (71, '15', 3),
    (72, '16', 3), (73, '17', 3),(74, '18', 3), (75, '19', 3), (76, '20', 3), 
    (77, '21', 3),(78, '22', 3), (79, '23', 3), (80, '24', 3), (81, '25', 3),
    (82, '26', 3), (83, '27', 3),(84, '28', 3), (85, '29', 3), (86, '30', 3),
    (87, '31', 3),(88, '32', 3), (89, '33', 3), (90, '34', 3), (91, '35', 3),
    (92, '36', 3), (93, '37', 3),(94, '38', 3), (95, '39', 3), (96, '40', 3), 
    (97, '41', 3),(98, '42', 3), (99, '43', 3), (100, '44', 3), (101, '45', 3),
     
    (102, '1', 4), (103, '2', 4), (104, '3', 4), (105, '4', 4), (106, '5', 4), 
    (107, '6', 4), (108, '7', 4), (109, '8', 4), (110, '9', 4), (111, '10', 4), 
    (112, '11', 4), (113, '12', 4), (114, '13', 4), (115, '14', 4), (116, '15', 4),
    (117, '16', 4), (118, '17', 4), (119, '18', 4), (120, '19', 4), (121, '20', 4), 
 
    (122, '1', 5), (123, '2', 5), (124, '3', 5), (125, '4', 5), (126, '5', 5), 
    (127, '6', 5), (128, '7', 5), (129, '8', 5), (130, '9', 5), (131, '10', 5), 
    (132, '11', 5), (133, '12', 5), (134, '13', 5), (135, '14', 5), (136, '15', 5), 
    (137, '16', 5), (138, '17', 5), (139, '18', 5), (140, '19', 5), (141, '20', 5), 
    (142, '21', 5), (143, '22', 5), (144, '23', 5), (145, '24', 5), (146, '25', 5), 
    (147, '26', 5), (148, '27', 5), (149, '28', 5), (150, '29', 5), (151, '30', 5), 
    (152, '31', 5), (153, '32', 5), (154, '33', 5), (155, '34', 5), (156, '35', 5), 
    (157, '36', 5), (158, '37', 5), (159, '38', 5), (160, '39', 5), (161, '40', 5), 
    (162, '41', 5), (163, '42', 5), (164, '43', 5), (165, '44', 5), (166, '45', 5), 
    (167, '46', 5), (168, '47', 5), (169, '48', 5), (170, '49', 5), (171, '50', 5),
    (172, '51', 5), (173, '52', 5), (174, '53', 5), (175, '54', 5), (176, '55', 5),
     
    (177, '1', 6), (178, '2', 6), (179, '3', 6), (180, '4', 6), (181, '5', 6), 
    (182, '6', 6), (183, '7', 6), (184, '8', 6), (185, '9', 6), (186, '10', 6), 
    (187, '11', 6), (188, '12', 6), (189, '13', 6);
COMMIT; 

-- -----------------------------------------------------
-- Data for table `bdfilateq`.`situacionjuridica`
-- -----------------------------------------------------

START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`situacionjuridica` (`id_situacionjur`, `nombresitjur`, `estado`) VALUES 
    (1, 'DISPONIBLE', 1),
    (2, 'EXTINTO', 1),
    (3, 'INVADIDO', 1),
    (4, 'JUICIO', 1),
    (5, 'PROCESO DE REGULARIZACIÓN', 1),
    (6, 'REGULARIZADO', 1),
    (7, 'VENTA', 1);
COMMIT;

-- -----------------------------------------------------
-- Data for table `bdfilateq`.`situacionfisica`
-- -----------------------------------------------------

START TRANSACTION;
USE `bdfilateq`;
INSERT INTO `bdfilateq`.`situacionfisica` (`id_situacionfis`, `nombresitfis`, `estado`) VALUES 
    (1, 'EN BREÑA', 1),
    (2, 'CERCADO', 1),
    (3, 'BALDÍO', 1),
    (4, 'CONSTRUIDO', 1);
COMMIT;