<!-- --------------- VERIFICA SI ESTA LOGEADO -------------- -->
<?php
session_start();
if(!(isset($_SESSION['admin_session']))){
    header("Location: Acceso.php?mensaje=1"); //MENSAJE1: "INICIE SESION PARA PODER VER LA PÁGINA"   
}
?>
<!-- ------------------------------------------------------- -->
<?php header("Content-Type: text/html;charset=utf-8"); ?><!-- PERMITE MOSTRAR Ñ Y ACENTOS-->
<?php $fechaActual=  date("d/m/Y");?> <!--MUESTRA FECHA Y HORA ACTUAL -->
<?php $hora = new DateTime(); $hora->setTimezone(new DateTimeZone('America/Mexico_City')); ?>
<!-- ------------ SE INCLUYEN LIBRERIAS A USAR ----------- -->
<?php require 'includes/EncabezadoMenu.php' ?>
<?php require 'includes/DeslizadorImg2.php' ?>
<!-- ------------------------------------------------------- -->
<!-- Inicio Principal -->
<div id="principal">
    <div id="principal-arriba"></div>
    <!-- Inicio Principal Centro -->
    <div id="principal-contenido">
        <!-- Inicio Barra_sitio -->
        <div id="barra_sitio">
            <div class="barra_sitio-exterior">
                <div class="barra_sitio-contenido">
                    <div class="centrar">   
                    <ul>
                        <li class="fichero_enlacevi�t">
                            <h2><a><?php echo $_SESSION['tipo_usuario'] ?></a></h2>                            
                        </li>                                            
                    </ul>                  
                    <div class="cl">&nbsp;</div> 
                    <img src="css/images/user_accept.png" /></p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    <p><h2><?php echo $_SESSION['admin_nombre'] ?></h2></p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    </div>
                </div>                 
            </div>            
            <div class="barra_sitio-abajo">&nbsp;</div>           
        </div>
        <!-- Fin Barra de Sitio -->
        <!-- Inicio Contenido -->
        <div id="contenido">
            <div class="post">
                <div class="titulo">
                    <h2><a title="Buscar">SISTEMA DE CONTROL INMOBILIARIO</a></h2>
                    <div class="cl">&nbsp;</div>                     
                </div>                
                <p class="post-info"><strong>D&Iacute;A:</strong> <?php echo $fechaActual ?>&nbsp;<strong>HORA:</strong> <?php echo $hora->format("g:i a"); ?> </p>
                <img src="css/images/archivero.png" />
                <div class="contenido">                    
                    <h4> SISTEMA DISE&Ntilde;ADO PARA EL CONTROL DE PREDIOS DEL LAGO DE TEQUESQUITENGO. </h4>
                </div>
                <div class="cl">&nbsp;</div>
                <a href="#" class="mas" title="Salir"><span class="separador">&nbsp;</span><span onclick="location='cerrarsesion.php'">SALIR</span></a>
                <div class="cl">&nbsp;</div>              
            </div>            
        </div>
        <!-- Fin Contenido -->
        <div class="cl">&nbsp;</div>        
    </div>
    <!-- Fin Principal Centro -->
    <div id="principal-abajo">&nbsp;</div>  
</div>
<!-- Fin Principal -->
<?php require 'includes/PieDePagina.php' ?>	