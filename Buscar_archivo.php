<?php
//------------------ SE INCLUYEN LIBRERIAS A USAR --------------------//
include_once 'config/Mysql.php';
include_once 'clases/Entrada.php';
include_once 'clases/FuncionesABD.php';
//---- REVISA QUE EXISTAN LOS VALORES ENVIADOS POR EL FORMULARIO ----//
if (isset($_POST['seccion']) && $_POST['manzana'] && $_POST['lote']){
    //SE OBTIENEN TODOS LOS DATOS ENVIADOS POR EL FORMULARIO
    $objDal=new FuncionesABD();
    $seccion=$_POST['seccion'];
    $manzana=$_POST['manzana'];
    $lote=$_POST['lote'];
    $resultado=$objDal->Buscar_archivo($seccion,$manzana,$lote);
 
    //SI SE OBTUVIERON DATOS
    if($resultado!=NULL ){
        session_start();
        $_SESSION['resultado_busqueda'] = $resultado;
        //REDIRECCIONA A (BUSCARRES.PHP) Y MUESTRA LOS RESULTADOS OBTENIDOS
        header("Location: BuscarRes.php");
    }else{
        //SE REDIRECCIONA A (BUSCAR.PHP) MENSAJE 0: "NO ENCONTRADO"
        header("Location: Buscar.php?mensaje=0");
    }    
}
//--------------------------------------------------------------------//
?>